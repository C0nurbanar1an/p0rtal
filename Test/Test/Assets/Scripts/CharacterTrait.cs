﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTrait  {

	int id;
	int name;
	int description;
	Attributes attributeModifiers;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public int Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public int Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

	public Attributes AttributeModifiers {
		get {
			return this.attributeModifiers;
		}
		set {
			attributeModifiers = value;
		}
	}

}
