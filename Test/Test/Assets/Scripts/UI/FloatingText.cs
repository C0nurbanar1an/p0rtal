﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingText : MonoBehaviour {

	public Animator animator;
	private Text text;

	void Awake(){
		AnimatorClipInfo[] clipInfo = this.animator.GetCurrentAnimatorClipInfo (0);
		Destroy (gameObject, clipInfo [0].clip.length);
		this.text = animator.gameObject.GetComponent<Text> ();
	}

	public void setText(string text){
		this.text.text = text;
	}

}
