﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBuffs  {

	Character character;
	List<Buff> buffs;

	/****   CONSTRUCTORES   ****/

	public CharacterBuffs(Character owner){
		this.buffs = new List<Buff> ();
		this.character = owner;
	}

	/****       METODOS     ****/

	public void onTimePassed(int seconds){
		//Avisamos tiempo pasado a los buffs.
		List <Buff> buffsToRemove = new List<Buff> ();
		foreach (Buff buff in this.buffs) {
			buff.onTimePassed (seconds);
			//Verificamos si hay que eliminarlo.
			//En caso positivo, lo agregamos a la lista de eliminacion.
			if (buff.hasEnded ()) {
				buffsToRemove.Add (buff);
			}
		}

		foreach (Buff buff in buffsToRemove) {
			//buff.applyConstantEffects ();
			buff.remove();
			this.removeBuff (buff);
		}

	}

	public void applyBuffConstantEffects(){
		foreach(Buff buff in this.buffs){
			buff.applyConstantEffects ();
		}
	}

	public void removeBuff(Buff buff){
		this.buffs.Remove (buff);
		//this.character.recalculateVirtues ();
		//Notify GUI.
		//this.character.updateAttributes();
	}

	public void addBuff(Buff buff){
		this.buffs.Add (buff);
		buff.onAddedToCharacter (this.character);
		buff.applyConstantEffects ();
		//this.character.updateAttributes ();
	}

	/**** SETTERS Y GETTERS ****/

	public Character Character {
		get {
			return this.character;
		}
		set {
			character = value;
		}
	}

	public List<Buff> Buffs {
		get {
			return this.buffs;
		}
		set {
			buffs = value;
		}
	}


}
