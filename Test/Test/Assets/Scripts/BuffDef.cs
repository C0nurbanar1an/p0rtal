﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffDef : Definition {

	public enum BuffType {
		CHEMICAL,
		MAGIC,
		PHYSICAL,
		EMOTIONAL,
		MENTAL,
		NULL
	}

	BuffType type;
	int baseDuration;
	float cureCostMultiplier;
	List<IterativeEffectDef> iterativeEffects;
	List<CharacterEffect> constantEffects;
	Sprite sprite;

	/****   CONSTRUCTORES   ****/

	public BuffDef (){
		this.constantEffects = new List<CharacterEffect>();
		this.iterativeEffects = new List<IterativeEffectDef>();
	}

	/****       METODOS     ****/



	public void addIterativeEffect(IterativeEffectDef effect){
	
		this.iterativeEffects.Add (effect);
	
	}

	public void addConstantEffect (CharacterEffect effect){

		this.constantEffects.Add (effect);

	}

	/**** SETTERS Y GETTERS ****/


	public BuffType Type {
		get {
			return this.type;
		}
		set {
			type = value;
		}
	}

	public int BaseDuration {
		get {
			return this.baseDuration;
		}
		set {
			baseDuration = value;
		}
	}

	public float CureCostMultiplier {
		get {
			return this.cureCostMultiplier;
		}
		set {
			cureCostMultiplier = value;
		}
	}

	public List<IterativeEffectDef> IterativeEffects {
		get {
			return this.iterativeEffects;
		}
		set {
			iterativeEffects = value;
		}
	}

	public List<CharacterEffect> ConstantEffects {
		get {
			return this.constantEffects;
		}
		set {
			constantEffects = value;
		}
	}

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			sprite = value;
		}
	}


	public class IterativeEffectDef {

		private int frequency; //Do it Every X minutes.
		//private ImmediateEffect effect;
		private CharacterEffect effect;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		/**** SETTERS Y GETTERS ****/


		public int Frequency {
			get {
				return this.frequency;
			}
			set {
				frequency = value;
			}
		}

		public CharacterEffect Effect {
			get {
				return this.effect;
			}
			set {
				effect = value;
			}
		}

	}

}
