﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat {

	/*
	Ver si ponerle una maquina de estados al GameManager o
	si darle control de la UI y el flujo el programa al Combat Manager.
	*/


	private List<Party> parties;
	private List<Character> characterTurnStack;
	private Character currentCharacter;
	private int currentRound;

	/****   CONSTRUCTORES   ****/

	public Combat(List<Party> parties){
		this.parties = parties;
		//this.startCombat();
	}

	/****       METODOS     ****/

	public void startCombat(){
		this.currentRound = 0;
		this.currentCharacter = null;

		this.startNewRound ();

	}

	private void refillCharactersAP(){
		foreach (Character c in this.CharacterTurnStack) {
			c.CharacterConditions.getCharacterCondition ("AP").refill ();
		}
	}

	public void goToNextCharacter(){
		this.selectNextCharacter ();
	}

	public void skipCurrentTurn(){
		this.selectNextCharacter ();
	}

	private void selectNextCharacter (){
		if (this.characterTurnStack.Count > 0) {

			bool nextCharacterFound = false;
			Character nextChar;
			while (!nextCharacterFound){

				if (this.characterTurnStack.Count == 0) {
					this.startNewRound();
					return;
				}

				nextChar = characterTurnStack [0];
				if (!nextChar.isDead() && !nextChar.isHardCCed()){
					this.currentCharacter = nextChar;
					nextCharacterFound = true;
				}

				this.characterTurnStack.RemoveAt (0);
				//Notify GameManager/GUI.
			}

		} else {
			//If none was selected, then it's time to go the next round.
			this.startNewRound ();
		}
	}

	public void finishCombat(){
		//Create Loots and return to normal game.
	}

	private bool hasCombatFinished(){
		//Condiciones de Victoria :


		List<Party> aliveParties = this.aliveParties();
		if (aliveParties.Count == 0){
			//Evaluar, empate o que se hace.
		} else if (aliveParties.Count == 1) {
			//Evaluamos de que bando es la party que sobrevivio:
			if (aliveParties[0].isPlayerOwned()){
				//Se murieron todos los enemigos.
				//You win!
			} else {
				//Se murieron todos los tuyos.
				//You lose!
			}
		}


		//Se cancela el modo combate

		//La party huye.

		return false;
	}

	public List<Party> aliveParties(){
		List<Party> parties = new List<Party>();
		foreach(Party p in this.parties){
			if (p.isAnyoneAlive()){
				parties.Add(p);
			}
		}
		return parties;
	}

	public void currentCharacterEndedTurn(){
		//Check end of turn stuff.
		this.goToNextCharacter();
	}

	private void startNewRound(){
		this.currentRound++;
		this.buildCharacterTurnStack ();
		this.currentCharacter = null;
		this.refillCharactersAP ();
		this.goToNextCharacter ();
	}

	private void buildCharacterTurnStack (){
		this.characterTurnStack = new List<Character> ();
		foreach(Party p in this.parties){
			foreach (Character character in p.Characters) {
				//Si esta vivo, lo reseteamos. Si no, lo ponemos como jugado asi no lo selecciona.
				if (character.isAlive ()) {
					this.characterTurnStack.Add (character);
				} 
			}
		}
		//Debug.LogWarning ("Total Characters in combat : " + this.characterTurnStack.Count);
		this.sortCharacterTurnStack ();
	}

	private void characterDied(){
		if (this.hasCombatFinished()){
			this.endCombat();
		}
	}

	public void characterRanOutOfAP(){
		this.goToNextCharacter();
	}

	public void combatHasBeenCancelled(){
		this.endCombat();
	}

	private void endCombat(){
		//Do Stuff
	}

	private void sortCharacterTurnStack(){
		IComparer<Character> comparer = new Character.InitiativeComparer();
		this.characterTurnStack.Sort (comparer);
	}

	/**** SETTERS Y GETTERS ****/

	public List<Party> Parties {
		get {
			return this.parties;
		}
		set {
			parties = value;
		}
	}

	public List<Character> CharacterTurnStack {
		get {
			return this.characterTurnStack;
		}
		set {
			characterTurnStack = value;
		}
	}

	public Character CurrentCharacter {
		get {
			return this.currentCharacter;
		}
		set {
			currentCharacter = value;
		}
	}

	public int CurrentRound {
		get {
			return this.currentRound;
		}
		set {
			currentRound = value;
		}
	}

}
