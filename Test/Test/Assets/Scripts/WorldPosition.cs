﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldPosition {

	World world;
	Position position;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public World World {
		get {
			return this.world;
		}
		set {
			world = value;
		}
	}

	public Position Position {
		get {
			return this.position;
		}
		set {
			position = value;
		}
	}


}