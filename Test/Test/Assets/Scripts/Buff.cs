﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff {

	private BuffDef buffDef;
	private Character caster;
	private Character target;
	private int remainingDuration;
	private int lvl;	
	private List<IterativeEffect> iterativeEffects;
	private bool isRunning = false;
	private Attributes alteredAttributes;
	private Skills alteredSkills;
	private bool removed = false;
	/****   CONSTRUCTORES   ****/

	public Buff (BuffDef buffDef, Character caster){
		this.buffDef = buffDef;
		this.caster = caster;
		this.remainingDuration = buffDef.BaseDuration;
		this.lvl = 1; //Ver de implementar tarde.
		this.alteredAttributes = new Attributes();
		this.alteredSkills = new Skills ();
		this.iterativeEffects = new List<IterativeEffect> ();
		this.initializeIterativeEffects();
	}

	/****       METODOS     ****/

	private void initializeIterativeEffects(){
		IterativeEffect ie;
		foreach (BuffDef.IterativeEffectDef ied in this.buffDef.IterativeEffects) {
			ie = new IterativeEffect ();
			ie.AccumulatedTime = 0;
			ie.IterativeEffectDef = ied;
			ie.Buff = this;
			this.iterativeEffects.Add (ie);
		}
	}

	public void onTimePassed(int seconds){
		if (this.isRunning && !this.hasEnded())  {

			if (this.buffDef.BaseDuration > -1) {
				int oldRemainingDuration = this.remainingDuration;

				this.reduceRemainingDuration (seconds);

				if (this.remainingDuration == 0) {
					seconds = oldRemainingDuration;
				}
			}

			foreach (IterativeEffect ie in this.iterativeEffects) {
				ie.onTimePassed (seconds);
			}

		}
	}

	public bool hasEnded(){
		return (this.remainingDuration == 0);
	}

	public void remove(){
		if (this.removed) {
			Debug.LogError ("Buff " + this.buffDef.Name + " has already been removed!");
			return;
		}
		foreach (Attribute attr in this.alteredAttributes.AttributeList) {
			attr.Value = -attr.Value;
			this.target.addExtraAttribute (attr);
		}

		foreach (Skill sk in this.alteredSkills.SkillList) {
			sk.Value = -sk.Value;
			this.target.addExtraSkill (sk);
		}

		this.removed = true;

	}

	private void reduceRemainingDuration(int seconds){
		this.remainingDuration -= seconds;
		if (remainingDuration <= 0) {
			this.remainingDuration = 0;
		}
	}

	public void onAddedToCharacter(Character target){
		this.target = target;
		this.startRunning ();
	}

	private void startRunning(){
		this.isRunning = true;
	}
			
	public int getCureCost(){
		return (int) (this.lvl * this.buffDef.CureCostMultiplier);
	}

	public void applyConstantEffects(){

		foreach(CharacterEffect constantEffect in this.buffDef.ConstantEffects){
			//Debug.LogWarning ("Applying Constant Effect : " + constantEffect.ToString ());
			if (constantEffect.Type == CharacterEffect.EffectType.ALTER_ATTRIBUTE) {
				AlterCharacterAttributeEffect alterAttributeEffect = (AlterCharacterAttributeEffect)constantEffect;
				this.alteredAttributes.addAttribute(alterAttributeEffect.getAttributeToAdd(this.caster,this.target));					
			}

			if (constantEffect.Type == CharacterEffect.EffectType.ALTER_SKILL) {
				AlterCharacterSkillEffect alterSkillEffect = (AlterCharacterSkillEffect)constantEffect;
				this.alteredSkills.addSkill(alterSkillEffect.getSkillToAdd(this.caster,this.target));					
			}

			constantEffect.apply(this.caster, this.target);
		}
	}

	/**** SETTERS Y GETTERS ****/

	public BuffDef BuffDef {
		get {
			return this.buffDef;
		}
		set {
			buffDef = value;
		}
	}

	public Character Caster {
		get {
			return this.caster;
		}
		set {
			caster = value;
		}
	}

	public int RemainingDuration {
		get {
			return this.remainingDuration;
		}
		set {
			remainingDuration = value;
		}
	}

	public int Lvl {
		get {
			return this.lvl;
		}
		set {
			lvl = value;
		}
	}

	public Character Target {
		get {
			return this.target;
		}
		set {
			target = value;
		}
	}

	public bool IsRunning {
		get {
			return this.isRunning;
		}
		set {
			isRunning = value;
		}
	}

	private class IterativeEffect {
		private BuffDef.IterativeEffectDef iterativeEffectDef;
		private Buff buff;
		private int accumulatedTime;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		public void onTimePassed(int seconds){
			this.accumulatedTime += seconds;
			while (this.accumulatedTime >= this.iterativeEffectDef.Frequency) {
				this.accumulatedTime -= this.iterativeEffectDef.Frequency;
				this.iterativeEffectDef.Effect.apply (this.buff.caster, this.buff.target);
			}
		}

		/**** SETTERS Y GETTERS ****/

		public BuffDef.IterativeEffectDef IterativeEffectDef {
			get {
				return this.iterativeEffectDef;
			}
			set {
				iterativeEffectDef = value;
			}
		}

		public int AccumulatedTime {
			get {
				return this.accumulatedTime;
			}
			set {
				accumulatedTime = value;
			}
		}

		public Buff Buff {
			get {
				return this.buff;
			}
			set {
				buff = value;
			}
		}

	}

}