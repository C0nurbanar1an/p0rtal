﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechService : Service {

	private Tech tech;


	/****   CONSTRUCTORES   ****/

	public TechService(Tech tech){
		this.tech = tech;
		this.minWorkers = 1;
	}

	/****       METODOS     ****/

	protected override void onCompleted (){
		//Notify
	}

	protected override void updateProgress(){
		this.addProgressToService (this.ProgressPerMinutes(1));
	}

	public int ProgressPerMinute (){
		
		int total = 0;
		foreach (Character c in this.workers){
			total += c.CharacterSkills.FinalSkills.getSkillByKey ("INV").Value;
		}
		return total;

	}

	protected override bool isCompleted(){
		return tech.ResearchCompleted;
	}

	protected override void addProgress(int progress){
		this.tech.addScience (progress);
	}

	/**** SETTERS Y GETTERS ****/

	public Tech Tech {
		get {
			return this.tech;
		}
		set {
			tech = value;
		}
	}

}
