﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController {

	protected const int LEFT_CLICK = 0;
	protected const int RIGHT_CLICK = 1;
	protected const int MIDDLE_CLICK = 2;

	protected UI ui;
	protected GameManager gameManager;

	//Always call first.

	public virtual void onUpCharacterPressed(){
		
	}	

	public virtual void onDownCharacterPressed(){
		
	}

	public virtual void onCombatButtonPressed(){
		
	}

	public virtual void onEndTurnButtonPressed(){

	}

	public virtual void onQuickAbilityButtonPressed(int index){	

	}

	public virtual void characterOnMouseEnter(CharacterMB characterMB){
			
	}

	public virtual void characterOnMouseExit(CharacterMB characterMB){
		
	}

	public virtual void onCharacterClicked(CharacterMB characterMB, int clickType){

	}

	protected virtual void start(){

	}

	protected virtual void update(){

	}

	protected virtual void updateUI(){

	}

	public virtual void finalize(){
	}

}
