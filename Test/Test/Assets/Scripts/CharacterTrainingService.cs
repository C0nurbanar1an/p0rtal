﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTrainingService : Service {

	/* Solo admite 1 Trabajador */

	AttributeDef attributeDef;

	/****   CONSTRUCTORES   ****/

	public CharacterTrainingService(AttributeDef attributeDef){
		this.attributeDef = attributeDef;
		this.minWorkers = 0;
	}

	/****       METODOS     ****/

	protected override void onCompleted (){
		//Notify
	}

	protected override void updateProgress(){
		this.addProgressToService (this.ProgressPerMinutes(1));
	}

	public int ProgressPerMinute (){

		return AttributeDef.BASE_TRAINING_PER_MINUTE;

	}

	protected override bool isCompleted(){
		return this.workers[0].CharacterAttributes.FinalAttributes.getAttributeById (this.attributeDef.Id).isMaxLvl();
	}

	protected override void addProgress(int progress){
		this.workers[0].CharacterAttributes.FinalAttributes.getAttributeById (this.attributeDef.Id).addExp (progress);
	}

	/**** SETTERS Y GETTERS ****/

	public AttributeDef AttributeDef {
		get {
			return this.attributeDef;
		}
		set {
			attributeDef = value;
		}
	}

}
