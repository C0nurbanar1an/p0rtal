﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBuffEffect : CharacterEffect {

	BuffDef buffDef;


	/****   CONSTRUCTORES   ****/

	public AddBuffEffect  (){
		this.type = EffectType.ADD_BUFF;
	}

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public BuffDef BuffDef {
		get {
			return this.buffDef;
		}
		set {
			buffDef = value;
		}
	}

}
