﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class AlterCharacterConditionEffect : CharacterEffect {

	CharacterConditionDef characterConditionDef;

	/****   CONSTRUCTORES   ****/

	public AlterCharacterConditionEffect(){
		this.type = EffectType.ALTER_CONDITION;
	}

	/****       METODOS     ****/

	public override void apply(Character caster, Character target){
		CharacterCondition cc = target.CharacterConditions.getCharacterCondition (this.characterConditionDef.Id);
		cc.alterValue (this.getDeltaValue (caster,target));

		//Debug.Log ("AlterCharacterCondition applied to " + target.Biography.Name);
	}

	public int getDeltaValue(Character caster, Character target){

		return this.formula.getFormulaValue(caster,target);

	}

	public override string ToString(){
	
		string result = "Alter Condition Effect : " + Environment.NewLine;
		result += "Condition : " + this.characterConditionDef.Key + Environment.NewLine;
		result += this.formula.ToString() + Environment.NewLine;
		return result;
	}
	/**** SETTERS Y GETTERS ****/


	public CharacterConditionDef CharacterConditionDef {
		get {
			return this.characterConditionDef;
		}
		set {
			characterConditionDef = value;
		}
	}


}
