﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemProperty  {

	protected ItemDef itemDef;

	public virtual void setItemDef(ItemDef itemDef){
		
	}

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public ItemDef ItemDef {
		get {
			return this.itemDef;
		}
		set {
			itemDef = value;
		}
	}

}
