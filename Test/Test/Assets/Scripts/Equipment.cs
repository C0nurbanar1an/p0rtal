﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment {

	//     Pensar bien el sistema de equipamientos, porque si hay que
	//     implementar equipamientos para cada raza, hay que hacerlo
	//     mas generico, probablemente usando composite para darle la
	//     flexibilidad que necesita.

	private HeadGear headGear;
	private Apparel apparel;
	private MainHand mainHand;
	private OffHand offHand;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public HeadGear HeadGear {
		get {
			return this.headGear;
		}
		set {
			headGear = value;
		}
	}

	public Apparel Apparel {
		get {
			return this.apparel;
		}
		set {
			apparel = value;
		}
	}

	public MainHand MainHand {
		get {
			return this.mainHand;
		}
		set {
			mainHand = value;
		}
	}

	public OffHand OffHand {
		get {
			return this.offHand;
		}
		set {
			offHand = value;
		}
	}

}
