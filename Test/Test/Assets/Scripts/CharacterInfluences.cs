﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class CharacterInfluences {

	private List<AttributeInfluence> attributeInfluences;
	private List<SkillInfluence> skillInfluences;
	private List<ConditionInfluence> conditionInfluences;
	//private int lvlMultiplier; //Implementar si hace falta.
	//private List<CharacterConditionInfluence> conditionInfluences;


	/****   CONSTRUCTORES   ****/

	public CharacterInfluences (){

		this.attributeInfluences = new List<AttributeInfluence> ();
		this.skillInfluences = new List<SkillInfluence> ();	
		this.conditionInfluences = new List<ConditionInfluence> ();

	}

	public bool hasInfluences(){
		return (this.attributeInfluences.Count +
		this.skillInfluences.Count +
		this.conditionInfluences.Count > 0);
	}

	public void addSkillInfluence(SkillInfluence ski){
		this.skillInfluences.Add (ski);
	}

	public void addAttributeInfluence(AttributeInfluence ai){
		this.attributeInfluences.Add (ai);
	}

	public void addConditionInfluence(ConditionInfluence ci){
		this.conditionInfluences.Add (ci);
	}

	/****       METODOS     ****/

	public int getCharacterInfluencesValue (Character character){

		float result = 0;

		foreach (AttributeInfluence ai in this.attributeInfluences) {
			result += ai.getCharacterInfluence (character);
		}

		foreach (SkillInfluence ski in this.skillInfluences) {
			result += ski.getCharacterInfluence (character);
		}

		foreach (ConditionInfluence ci in this.conditionInfluences) {
			result += ci.getCharacterInfluence (character);
		}

		return (int) result;

	}

	public override string ToString ()
	{
		string result = "";

		if (this.attributeInfluences.Count > 0) {
			//result += "AttributeInfluences : " + Environment.NewLine;
			foreach (AttributeInfluence ai in this.attributeInfluences) {
				result += ai.toSmallString () + Environment.NewLine;
			}
		}

		if (this.skillInfluences.Count > 0) {
			//result += "Skill Influences : " + Environment.NewLine;
			foreach (SkillInfluence ski in this.skillInfluences) {
				result += ski.toSmallString () + Environment.NewLine;
			}
		}

		if (this.conditionInfluences.Count > 0) {
			//result += "Condition Influences : " + Environment.NewLine;
			foreach (ConditionInfluence ci in this.conditionInfluences) {
				result += ci.toSmallString () + Environment.NewLine;
			}
		}
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public List<AttributeInfluence> AttributeInfluences {
		get {
			return this.attributeInfluences;
		}
		set {
			attributeInfluences = value;
		}
	}

	public List<SkillInfluence> SkillInfluences {
		get {
			return this.skillInfluences;
		}
		set {
			skillInfluences = value;
		}
	}

	public List<ConditionInfluence> ConditionInfluences {
		get {
			return this.conditionInfluences;
		}
		set {
			conditionInfluences = value;
		}
	}

}
