﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;


public class ConditionInfluence {

	public enum BoundaryType {
		MAX,
		CURRENT,
		NULL
	}

	private CharacterConditionDef conditionDef;
	private BoundaryType boundary;
	private float multiplier;

	/****   CONSTRUCTORES   ****/

	public ConditionInfluence (BoundaryType boundaryType,
		CharacterConditionDef conditionDef,
		int multiplier){

		this.boundary = boundaryType;
		this.multiplier = multiplier;
		this.conditionDef = conditionDef;

	}

	public ConditionInfluence(){

	}
	/****       METODOS     ****/

	public float getCharacterInfluence(Character character){

		CharacterConditions conditions = character.CharacterConditions;
		CharacterCondition condition = conditions.getCharacterCondition (this.conditionDef.Id);
		if (condition != null) {
			return this.getProperConditionValue (condition) * multiplier;

		} else {
			return 0;
			//Debug.LogError ("Attribute " + this.attributeDef.Key + " Not Found on Character " + character.ToString);
		}

	}

	private int getProperConditionValue(CharacterCondition condition){
		int properValue;

		switch (this.boundary) {

		case BoundaryType.CURRENT:
			properValue = condition.Value;
			break;
		case BoundaryType.MAX:
			properValue = condition.MaxValue;
			break;
		default: 
			properValue = 0; //Error.
			Debug.LogError ("Invalid Boundary Type! [" + this.boundary + "]");
			break;
		}

		return properValue;

	}

	public override string ToString ()
	{
		string result = "ConditionInfluence : " + Environment.NewLine;
		result += "ConditionDef : " + this.conditionDef.Key + Environment.NewLine;
		result += "Status : " + Enum.GetName (typeof(BoundaryType), this.boundary) + Environment.NewLine;
		result += "Multiplier : " + this.multiplier + Environment.NewLine;
		return result;
	}

	public string toSmallString(){

		return "[COND] (" + Enum.GetName (typeof(BoundaryType), this.boundary) + ") " + this.conditionDef.Key + " X " + this.multiplier;

	}

	/**** SETTERS Y GETTERS ****/

	public CharacterConditionDef ConditionDef {
		get {
			return this.conditionDef;
		}
		set {
			conditionDef = value;
		}
	}

	public BoundaryType Boundary {
		get {
			return this.boundary;
		}
		set {
			boundary = value;
		}
	}

	public float Multiplier {
		get {
			return this.multiplier;
		}
		set {
			multiplier = value;
		}
	}




}
