﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCraftService : Service {

	private ItemDef itemDef;
	private int progressGoal;
	private int progress;
	private Item finalItem;

	/****   CONSTRUCTORES   ****/

	public ItemCraftService(ItemDef itemDef){
		this.progressGoal = itemDef.ResourceConversion.getCraftCost();
		this.progress = 0;
		this.finalItem = null;
		this.minWorkers = 1;
	}

	/****       METODOS     ****/

	protected override void onCompleted (){
		this.finalItem = this.generateItem ();
		//Notify.
	}

	private Item generateItem(){
		//Revisar.
		return new Item (null);
	}

	protected override void updateProgress(){
		this.addProgressToService (this.ProgressPerMinutes(1));
	}

	public override int progressPerMinute(){
		int total = 0;
		foreach (Character c in this.workers){
			total += c.CharacterSkills.FinalSkills.getSkillByKey ("CRF").Value;
		}
		return total;
	}

	protected override bool isCompleted(){
		return (this.progress >= this.progressGoal);
	}

	protected override void addProgress(int progress){
		this.progress += progress;
	}
		

	/**** SETTERS Y GETTERS ****/

	public ItemDef ItemDef {
		get {
			return this.itemDef;
		}
		set {
			itemDef = value;
		}
	}

	public int ProgressGoal {
		get {
			return this.progressGoal;
		}
		set {
			progressGoal = value;
		}
	}

	public int Progress {
		get {
			return this.progress;
		}
		set {
			progress = value;
		}
	}

	public Item FinalItem {
		get {
			return this.finalItem;
		}
		set {
			finalItem = value;
		}
	}

}
