﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot  {
	Sprite sprite;
	List<Item> items;

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			sprite = value;
		}
	}

	public List<Item> Items {
		get {
			return this.items;
		}
		set {
			items = value;
		}
	}
}
