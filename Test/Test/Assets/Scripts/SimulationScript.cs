﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimulationScript : MonoBehaviour {

	Character character1;
	Character character2;
	Character character3;
	Character character4;
	Party party1;
	Party party2;

	public Character currentCharacter;
	public Character targetCharacter;

	DefinitionManager definitionManager;
	public LayerMask lm;
	public LayerMask lmGround;
	public Text hpText;
	public Text mpText;
	public Text logText;
	public bool targetMode;
	public Vector3 transformCurrent;
	public GameObject currentGameObject;
	public FollowCamera cameraScript;
	public Vector3 targetPosition;
	public GameManager gameManager;
	public GameObject targetforPosition;

	float timeCounter = 0;

	// Use this for initialization
	public void castSkill(int skill, Character caster, Character target){
		caster.castAbility(new Ability(this.definitionManager.getAbilityDef (skill)), target);

	}

	void Start () {
		this.initialize ();
		lm = LayerMask.GetMask ("Character");
		lmGround = LayerMask.GetMask ("Ground");

		hpText = GameObject.Find ("HpText").GetComponent<Text> ();
	    mpText = GameObject.Find ("MpText").GetComponent<Text> ();
		//logText = GameObject.Find ("LogScreen").GetComponent<Text> ();
		this.cameraScript = GameObject.Find ("Main Camera").GetComponent<FollowCamera> ();
		targetMode = false;

		targetforPosition = GameObject.Find ("target");
		this.gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}

	void Update () {

		if(currentCharacter != null){
			hpText.	text = "HP: " + currentCharacter.CharacterConditions.getCharacterCondition ("HP").Value; 
			mpText.text = "MP: " + currentCharacter.CharacterConditions.getCharacterCondition ("MP").Value; 
		}
		/*
		if (Input.GetMouseButtonDown (0) && !targetMode) {
				
			RaycastHit hitInfo = new RaycastHit ();
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo, 9999, lm.value)) {

				currentCharacter = hitInfo.collider.gameObject.GetComponent<CharacterMB> ().Character;
			
				this.cameraScript.currentGameObjectChanged(hitInfo.collider.gameObject);

				hpText.text = "HP: " + currentCharacter.CharacterConditions.getCharacterCondition ("HP").Value; 
				mpText.text = "MP: " + currentCharacter.CharacterConditions.getCharacterCondition ("MP").Value; 
			}
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo, 9999, lmGround.value)) { //Click MOve 
				 targetPosition = hitInfo.point;
			}
				
		}		
			
		if ((Input.GetKeyDown ("1") || targetMode) && currentCharacter!=null ) {
			targetMode = true;
			Debug.Log ("Select a target");
			if(Input.GetMouseButtonDown (0)){
				RaycastHit hitInfo = new RaycastHit ();
				if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo, 9999, lm.value)) {
					targetCharacter = hitInfo.collider.gameObject.GetComponent<CharacterMB> ().Character;

					castSkill (1, currentCharacter, targetCharacter);
				    Debug.Log (targetCharacter.CharacterConditions.getCharacterCondition ("HP").Value);
					targetMode = false;
				}
			}
		}*/
	}

	public void onTimePassed (int seconds){
		character1.onTimePassed (seconds);
		character2.onTimePassed (seconds);
		character3.onTimePassed (seconds);
		character4.onTimePassed (seconds);
	}

	/*void OnGUI() {
		CharacterAttributes attributes1 = this.character1.CharacterAttributes;
		CharacterAttributes attributes2 = this.character2.CharacterAttributes;

		int character1TextX = 10;
		int character2TextX = 350;
		//int buttonX = 550;

		GUI.Label(new Rect(character1TextX , 10, 300, 20), this.character1.Biography.Name);
		GUI.Label(new Rect(character1TextX , 30, 100, 20), "HP : " + this.character1.CharacterConditions.getCharacterCondition("HP").Value + " / " + this.character1.CharacterConditions.getCharacterCondition("HP").MaxValue) ;
		GUI.Label(new Rect(character1TextX , 50, 100, 20), "MP : " + this.character1.CharacterConditions.getCharacterCondition("MP").Value + " / " + this.character1.CharacterConditions.getCharacterCondition("MP").MaxValue);
		GUI.Label(new Rect(character1TextX , 80, 100, 20), "Attributes : ");
		GUI.Label (new Rect (character1TextX , 100, 500, 200), character1.CharacterAttributes.getFinalAttributeList());

		GUI.Label (new Rect (character1TextX , 100, 100, 20), "STR : " + attributes1.FinalAttributes.getAttributeByKey("STR").Value);
		GUI.Label (new Rect (character1TextX , 120, 100, 20), "AGI : " + attributes1.FinalAttributes.getAttributeByKey("AGI").Value);
		GUI.Label (new Rect (character1TextX , 140, 100, 20), "DEX : " + attributes1.FinalAttributes.getAttributeByKey("DEX").Value);
		GUI.Label (new Rect (character1TextX , 160, 100, 20), "CON : " + attributes1.FinalAttributes.getAttributeByKey("CON").Value);
		GUI.Label (new Rect (character1TextX , 180, 100, 20), "CHA : " + attributes1.FinalAttributes.getAttributeByKey("CHA").Value);
		GUI.Label (new Rect (character1TextX , 200, 100, 20), "INT : " + attributes1.FinalAttributes.getAttributeByKey("INT").Value);
		
		GUI.Label (new Rect (character1TextX , 220, 100, 20), "Skills : ");
		GUI.Label (new Rect (character1TextX , 240, 100, 200), character1.CharacterSkills.getFinalSkillList());

		GUI.Label(new Rect(character2TextX, 10, 300, 20), this.character2.Biography.Name);
		GUI.Label(new Rect(character2TextX, 30, 100, 20), "HP : " + this.character2.CharacterConditions.getCharacterCondition("HP").Value + " / " + this.character2.CharacterConditions.getCharacterCondition("HP").MaxValue);
		GUI.Label(new Rect(character2TextX, 50, 100, 20), "MP : " + this.character2.CharacterConditions.getCharacterCondition("MP").Value + " / " + this.character2.CharacterConditions.getCharacterCondition("MP").MaxValue);
		GUI.Label(new Rect(character2TextX, 80, 100, 20), "Attributes : ");
		GUI.Label (new Rect (character2TextX , 100, 100, 200), character2.CharacterAttributes.getFinalAttributeList());
		
		GUI.Label (new Rect (character2TextX, 100, 100, 20), "STR : " + attributes2.FinalAttributes.getAttributeByKey("STR").Value);
		GUI.Label (new Rect (character2TextX, 120, 100, 20), "AGI : " + attributes2.FinalAttributes.getAttributeByKey("AGI").Value);
		GUI.Label (new Rect (character2TextX, 140, 100, 20), "DEX : " + attributes2.FinalAttributes.getAttributeByKey("DEX").Value);
		GUI.Label (new Rect (character2TextX, 160, 100, 20), "CON : " + attributes2.FinalAttributes.getAttributeByKey("CON").Value);
		GUI.Label (new Rect (character2TextX, 180, 100, 20), "CHA : " + attributes2.FinalAttributes.getAttributeByKey("CHA").Value);
		GUI.Label (new Rect (character2TextX, 200, 100, 20), "INT : " + attributes2.FinalAttributes.getAttributeByKey("INT").Value);

		GUI.Label (new Rect (character2TextX , 220, 100, 20), "Skills : ");
		GUI.Label (new Rect (character2TextX , 240, 100, 200), character2.CharacterSkills.getFinalSkillList());

		/*if (GUI.Button (new Rect (buttonX, 10, 300, 50), "NACHO > FIREBALL a ROY")) {
			
		}

		if (GUI.Button (new Rect (buttonX, 70, 300, 50), "ROY > MORTAL STRIKE a NACHO ")) {
			this.definitionManager.getAbilityDef ("MRTLWND").cast (this.character2, this.character1);
		}
	}
	*/
	private void initialize(){
		this.gameManager = GameObject.Find ("GameManager").GetComponent<GameManager>();
		this.definitionManager = this.gameManager.DefinitionManager;

		this.createNachoCharacter ();
		this.createRoyCharacter ();
		this.createPotaCharacter ();
		this.createGonzaCharacter ();
		this.createParties ();
		this.gameManager.setCurrentParty (this.party2);
		this.gameManager.setCurrentCharacter (this.character3);

	}

	public void  createNachoCharacter(){

		this.character1 = new Character (definitionManager);
		this.character1.Biography.Name = "Nacho, Devourer of Galaxies";
		this.character1.addBuff(new Buff(definitionManager.getBuffDef("NACHOCUN"),this.character1));
		this.character1.Biography.Portrait = DefinitionManager.loadSprite ("warcraftSprites2_101");
		this.character1.instantiateOnScene (new Vector3 (-5, 1, -5));
	}

	public void createRoyCharacter(){
		this.character2 = new Character (definitionManager);
		this.character2.Biography.Name = "Roy, Hope's End";
		this.character2.addBuff(new Buff(definitionManager.getBuffDef("ROYRES"),this.character2));
		this.character2.Biography.Portrait = DefinitionManager.loadSprite ("warcraftSprites4_111");
		this.character2.instantiateOnScene (new Vector3 (0, 1, 0));
		this.character2.CharacterConditions.getCharacterCondition ("HP").substractValue (60);
	}

	public void createPotaCharacter(){
		this.character3 = new Character (definitionManager);
		this.character3.Biography.Name = "Pota, Harbinger of Knowledge";
		this.character3.addBuff(new Buff(definitionManager.getBuffDef("POTAACU"),this.character3));
		this.character3.Biography.Portrait = DefinitionManager.loadSprite ("warcraftSprites2_145");
		this.character3.instantiateOnScene (new Vector3 (5, 1, 5));
	}

	public void createGonzaCharacter(){
		this.character4 = new Character (definitionManager);
		this.character4.Biography.Name = "Gonza, Justice's Gladiator";
		this.character4.addBuff(new Buff(definitionManager.getBuffDef("GONZAPWR"),this.character4));
		this.character4.Biography.Portrait = DefinitionManager.loadSprite ("warcraftSprites2_90");
		this.character4.instantiateOnScene (new Vector3 (-5, 1, 5));
	}

	public void createParties(){
		this.party1 = new Party ();
		this.party1.tryToAddCharacter (character1);
		this.party1.tryToAddCharacter (character2);

		this.party2 = new Party ();
		this.party2.tryToAddCharacter (character3);
		this.party2.tryToAddCharacter (character4);

	}

	public void startCombat(){
		
	}

	public Building createTestBuilding(){
		return null;
	}





	public Character Character1 {
		get {
			return this.character1;
		}
		set {
			character1 = value;
		}
	}

	public Character Character2 {
		get {
			return this.character2;
		}
		set {
			character2 = value;
		}
	}

	public Character Character3 {
		get {
			return this.character3;
		}
		set {
			character3 = value;
		}
	}

	public Character Character4 {
		get {
			return this.character4;
		}
		set {
			character4 = value;
		}
	}

	public Party Party1 {
		get {
			return this.party1;
		}
		set {
			party1 = value;
		}
	}

	public Party Party2 {
		get {
			return this.party2;
		}
		set {
			party2 = value;
		}
	}
}
