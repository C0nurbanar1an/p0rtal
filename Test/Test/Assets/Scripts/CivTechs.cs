﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CivTechs  {

	private List<Tech> techs;


	/****   CONSTRUCTORES   ****/



	/****       METODOS     ****/

	public bool hasTech(TechDef techDef){
		return (this.getTech(techDef) != null);
	}

	public Tech getTech(TechDef techDef){
		foreach (Tech t in this.techs){
			if (t.TechDef.Id == techDef.Id){
				return t;
			}
		}
		return null;
	}

	public void addTech(Tech tech){
		if (!this.hasTech (tech.TechDef)){
			this.techs.Add (tech);
		}
	}

	public bool canBeResearched(TechDef techDef){
		foreach (TechDef reqTech in techDef.RequiredTechs){
			if (!this.hasTech(reqTech)){
				return false;
			}
		}
		return true;
	}

	/**** SETTERS Y GETTERS ****/

	public List<Tech> Techs {
		get {
			return this.techs;
		}
		set {
			techs = value;
		}
	}

}
