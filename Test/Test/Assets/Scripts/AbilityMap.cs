﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityMap : MonoBehaviour {

	List<List<AbilityDef>> abilitiesPerLvl;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public List<List<AbilityDef>> AbilitiesPerLvl {
		get {
			return this.abilitiesPerLvl;
		}
		set {
			abilitiesPerLvl = value;
		}
	}

}
