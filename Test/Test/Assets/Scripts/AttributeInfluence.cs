﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class AttributeInfluence : CharacterVirtueInfluence  {

	private AttributeDef attributeDef;
	private float multiplier;


	/****   CONSTRUCTORES   ****/

	public AttributeInfluence (VirtueStatusEnum virtueStatus,
						       AttributeDef attributeDef,
							   int multiplier){

		this.virtueStatus = virtueStatus;
		this.multiplier = multiplier;
		this.attributeDef = attributeDef;

	}

	public AttributeInfluence(){

	}

	/****       METODOS     ****/

	public override float getCharacterInfluence(Character character){
		float result = 0;
		Attributes attributes = getProperAttributesFromCharacter (character);
		Attribute attr = attributes.getAttributeById(this.attributeDef.Id);
		if (attr != null) {
			result = attr.Value * this.multiplier;
		} else {
			Debug.LogError ("Attribute " + this.attributeDef.Key + " Not Found on Character " + character.ToString());
		}
		return result;
	}

	private Attributes getProperAttributesFromCharacter(Character character){
		Attributes properAttributes;
		switch(this.virtueStatus){
		case VirtueStatusEnum.BASE:
			properAttributes = character.CharacterAttributes.BaseAttributes;
			break;
		case VirtueStatusEnum.EXTRA:
			properAttributes = character.CharacterAttributes.ExtraAttributes;
			break;
		case VirtueStatusEnum.FINAL:
			properAttributes = character.CharacterAttributes.FinalAttributes;
			break;
		default:
			properAttributes = null; //Error.
			Debug.LogError ("invalid virtueStatus!");
			break;
		}

		return properAttributes;

	}

	public override string ToString ()
	{
		string result = "Attribute Influence : " + Environment.NewLine;
		result += "AttributeDef : " + this.attributeDef.Key + Environment.NewLine;
		result += "Status : " + Enum.GetName (typeof(VirtueStatusEnum), this.virtueStatus) + Environment.NewLine;
		result += "Multiplier : " + this.multiplier + Environment.NewLine;
		return result;
	}

	public string toSmallString(){

		return "[ATTR] (" + Enum.GetName (typeof(VirtueStatusEnum), this.virtueStatus) + ") " + this.attributeDef.Key + " X " + this.multiplier;

	}

	/**** SETTERS Y GETTERS ****/

	public AttributeDef AttributeDef {
		get {
			return this.attributeDef;
		}
		set {
			attributeDef = value;
		}
	}

	public float Multiplier {
		get {
			return this.multiplier;
		}
		set {
			multiplier = value;
		}
	}


}
