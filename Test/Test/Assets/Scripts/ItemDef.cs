﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDef{

	string name;
	string description;
	string flavorText;
	Fabric fabric;
	int maxDurability;
	int weight;
	int maxStackSize;
	WeaponProperties weaponProperties;
	ToolProperties toolProperties;
	ConsumableProperties consumableProperties;
	ArmorProperties armorProperties;
	EquippableProperties equippableProperties;
	WeaponAttachmentProperties weaponAttachmentProperties;
	ResourceList resourceConversion;


	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

	public string FlavorText {
		get {
			return this.flavorText;
		}
		set {
			flavorText = value;
		}
	}

	public Fabric Fabric {
		get {
			return this.fabric;
		}
		set {
			fabric = value;
		}
	}

	public int MaxDurability {
		get {
			return this.maxDurability;
		}
		set {
			maxDurability = value;
		}
	}

	public int Weight {
		get {
			return this.weight;
		}
		set {
			weight = value;
		}
	}

	public int MaxStackSize {
		get {
			return this.maxStackSize;
		}
		set {
			maxStackSize = value;
		}
	}

	public WeaponProperties WeaponProperties {
		get {
			return this.weaponProperties;
		}
		set {
			weaponProperties = value;
		}
	}

	public ToolProperties ToolProperties {
		get {
			return this.toolProperties;
		}
		set {
			toolProperties = value;
		}
	}

	public ConsumableProperties ConsumableProperties {
		get {
			return this.consumableProperties;
		}
		set {
			consumableProperties = value;
		}
	}

	public ArmorProperties ArmorProperties {
		get {
			return this.armorProperties;
		}
		set {
			armorProperties = value;
		}
	}

	public EquippableProperties EquippableProperties {
		get {
			return this.equippableProperties;
		}
		set {
			equippableProperties = value;
		}
	}

	public WeaponAttachmentProperties WeaponAttachmentProperties {
		get {
			return this.weaponAttachmentProperties;
		}
		set {
			weaponAttachmentProperties = value;
		}
	}

	public ResourceList ResourceConversion {
		get {
			return this.resourceConversion;
		}
		set {
			resourceConversion = value;
		}
	}

}

