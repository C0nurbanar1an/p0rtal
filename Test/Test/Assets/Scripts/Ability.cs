﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability  {


	AbilityDef abilityDef;

	Dictionary<CharacterConditionDef,int> conditionCost;
	int cooldown;
	int remainingCooldown;

	/****   CONSTRUCTORES   ****/

	public Ability(AbilityDef abilityDef){

		this.abilityDef = abilityDef;
		this.remainingCooldown = 0;
		this.cooldown = this.abilityDef.BaseCooldown;
		this.conditionCost = this.abilityDef.CharacterConditionCost;

	}
		
	/****       METODOS     ****/

	public void cast(Character caster, Character target){
		this.abilityDef.cast (caster, target);
		this.remainingCooldown = this.cooldown;
	}

	public void alterRemainingCooldown(int value){
		if (value > 0) {
			this.addRemainingCooldown (value);
		} else {
			this.reduceRemainingCooldown (-value);
		}
			
	}

	public void reduceRemainingCooldown(int value){
		this.remainingCooldown -= value;
		if (this.remainingCooldown < 0) {
			this.remainingCooldown = 0;
			//Notify ability cd reached zero.
		}
	}

	public void reduceRemainingCooldown() {
		this.reduceRemainingCooldown (1);	
	}

	public void addRemainingCooldown(int value){
		this.remainingCooldown += value;
		if (this.remainingCooldown > this.cooldown) {
			this.remainingCooldown = this.cooldown;
		}
	}

	/**** SETTERS Y GETTERS ****/

	public AbilityDef AbilityDef {
		get {
			return this.abilityDef;
		}
		set {
			abilityDef = value;
		}
	}

	public Dictionary<CharacterConditionDef, int> ConditionCost {
		get {
			return this.conditionCost;
		}
		set {
			conditionCost = value;
		}
	}

	public int Cooldown {
		get {
			return this.cooldown;
		}
		set {
			cooldown = value;
		}
	}

	public int RemainingCooldown {
		get {
			return this.remainingCooldown;
		}
		set {
			remainingCooldown = value;
		}
	}

}
