﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Schema;
using System;

public class UI : MonoBehaviour {

	public static int RED = 0;
	public static int GREEN = 1;
	public static int BLUE = 2;

	private GameManager gameManager;
	private UIConsole console;
	private GameController controller;
	//**********************************//
	private Text targetHPText;
	private Text targetMPText;
	private Text targetAPText;
	private Text targetHPLabel;
	private Text targetMPLabel;
	private Text targetAPLabel;
	private Text targetNameText;
	private GameObject targetPortraitImageGO;
	private Image targetPortraitImage;
	private GameObject targetPanel;
	private GameObject currentCharacterPortraitImageGO;
	private Image currentCharacterPortraitImage;
	private GameObject partyPanel;
	private GameObject combatButton;
	private GameObject endTurnButton;
	private CharacterMB targetCharacterMB;

	//Floating Text
	private static FloatingText floatingText;
	private static GameObject canvas;
	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public static void initialize(){

	}

	public static void createFloatingText(string text,Transform location){
		FloatingText instance = Instantiate (floatingText);
		Vector2 screenPosition = Camera.main.WorldToScreenPoint (location.position);
		instance.transform.SetParent (canvas.transform, false);
		instance.transform.position = screenPosition;
		//instance.transform.position = location.position;
		instance.setText (text);

	}
	/*
	public static void createFloatingText(string text, Transform parent){
		FloatingText instance = Instantiate (floatingText);
		//Vector2 screenPosition = Camera.main.WorldToScreenPoint (location.position);

		instance.transform.SetParent (parent, false);
		//instance.transform.position = screenPosition;
		instance.transform.position = new Vector3(0,0,0);
		instance.setText (text);

	}*/

	public void hideEndTurnButton(){
		this.endTurnButton.SetActive (false);
	}

	public void showEndTurnButton(){
		this.endTurnButton.SetActive (true);
	}

	public void onUpCharacterPressed(){
		//this.gameManager.CurrentController.selectNextPartyCharacter ();
		this.gameManager.CurrentController.onUpCharacterPressed();
	}	

	public void onDownCharacterPressed(){
		this.gameManager.CurrentController.onDownCharacterPressed ();
	}
			
	public void onCombatButtonPressed(){
		this.gameManager.CurrentController.onCombatButtonPressed();
	}

	public void onEndTurnButtonPressed(){
		this.gameManager.CurrentController.onEndTurnButtonPressed();
	}

	public void onQuickAbilityButtonPressed(int index){
		//this.gameManager.onAbilityReadied ();
		this.gameManager.CurrentController.onQuickAbilityButtonPressed(index);

	}

	public void setCombatModeButtonSprite(Sprite sprite){
		this.combatButton.GetComponent<Image>().sprite = sprite;
	}

	public void printConsoleMsg(string msg){
		if (this.console != null) {
			this.console.printMessage (msg);
		}
		else {
			Debug.Log("Console still not assigned to UI.");
		}
	}
		
	void Awake(){
		this.gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		this.console = GameObject.Find ("Console").GetComponent<UIConsole> ();
		this.assignUIReferences ();
	}

	void Start () {
		
		//this.gameManager.CurrentController = gameManager.CurrentController;

		this.hideTargetPanel ();
	}

	private void assignUIReferences (){
		this.targetHPText = GameObject.Find ("TargetHP").GetComponent<Text> ();
		this.targetMPText = GameObject.Find ("TargetMP").GetComponent<Text> ();
		this.targetAPText = GameObject.Find ("TargetAP").GetComponent<Text> ();
		this.targetHPLabel = GameObject.Find ("TargetHPLabel").GetComponent<Text> ();
		this.targetMPLabel = GameObject.Find ("TargetMPLabel").GetComponent<Text> ();
		this.targetAPLabel = GameObject.Find ("TargetAPLabel").GetComponent<Text> ();
		this.targetNameText = GameObject.Find ("TargetName").GetComponent<Text> ();
		this.targetPortraitImageGO = GameObject.Find ("TargetPortraitImage");
		this.targetPortraitImage = this.targetPortraitImageGO.GetComponent<Image> ();
		this.targetPanel = GameObject.Find ("TargetPanel");
		this.combatButton = GameObject.Find ("CombatBtn");
		this.endTurnButton = GameObject.Find ("EndTurnBtn");
		canvas = GameObject.Find ("UI");
		floatingText = Resources.Load<FloatingText> ("prefabs/FloatingTextParent");
		//UI Script


		this.currentCharacterPortraitImageGO = GameObject.Find ("CharacterPortrait");
		this.currentCharacterPortraitImage = this.currentCharacterPortraitImageGO.GetComponent<Image>();
		this.partyPanel = GameObject.Find("PartyPanel");

	}

	public void updatePartyPanel(){
		if (this.gameManager != null) {
			if (this.gameManager.CurrentCharacter != null) {
				Sprite characterPortrait = this.gameManager.CurrentCharacter.Biography.Portrait;

				if (characterPortrait != null) {
					this.currentCharacterPortraitImageGO.SetActive (true);
					this.currentCharacterPortraitImage.sprite = characterPortrait;
				} else {
					this.currentCharacterPortraitImageGO.SetActive (false);
					this.currentCharacterPortraitImage.sprite = this.gameManager.DefinitionManager.defaultTargetPortrait;
				}
			}
		}
	}

	private void updateTargetPanel(){
		if (this.targetCharacterMB != null) {
			Character target = this.targetCharacterMB.Character;
			CharacterCondition hpCondition = target.CharacterConditions.getCharacterCondition ("HP");
			CharacterCondition mpCondition = target.CharacterConditions.getCharacterCondition ("MP");
			CharacterCondition apCondition = target.CharacterConditions.getCharacterCondition ("AP");
			Sprite targetPortrait = target.Biography.Portrait;
			this.targetNameText.text = target.Biography.Name;
			this.targetHPText.text = hpCondition.Value + " / " + hpCondition.MaxValue;
			this.targetMPText.text = mpCondition.Value + " / " + mpCondition.MaxValue;
			this.targetAPText.text = apCondition.Value + " / " + apCondition.MaxValue;
			if (targetPortrait != null) {
				this.targetPortraitImageGO.SetActive (true);
				this.targetPortraitImage.sprite = targetPortrait;
			} else {
				this.targetPortraitImageGO.SetActive (false);
				this.targetPortraitImage.sprite = this.gameManager.DefinitionManager.defaultTargetPortrait;
			}
		}
	}
		
	public void royPanel(){
		if (false) {
			if (this.targetCharacterMB != null) { //ATENTO aca Roy, te lo comente porque daba siempre null.
				Character target = this.targetCharacterMB.Character;
				CharacterCondition hpCondition = target.CharacterConditions.getCharacterCondition ("HP");
				//CharacterCondition spCondition = this.gameManager.Target.CharacterConditions.getCharacterCondition ("SP");
				//CharacterCondition saCondition = this.gameManager.Target.CharacterConditions.getCharacterCondition ("SA");
				ConditionBar hpBar = GameObject.Find ("VisualHp").GetComponent<ConditionBar> ();
				//ConditionBar spBar = GameObject.Find ("VisualSp").GetComponent<ConditionBar>();
				//ConditionBar saBar = GameObject.Find ("VisualSa").GetComponent<ConditionBar>();
				hpBar.showValue (hpCondition);
				//spBar.showValue (spCondition);    
				//saBar.showValue (saCondition);
			}
		}
	} 


	private void fillTargetPanelWithCharacter(Character character){
		this.showTargetPanel ();
	}

	public void hideTargetPanel(){
		this.targetPanel.SetActive (false);
	}

	public void showTargetPanel(CharacterMB characterMB){
		this.targetCharacterMB = characterMB;
		this.targetPanel.SetActive (true);
	}

	public void showTargetPanel(){
		this.targetPanel.SetActive (true);
	}

	/*
	public void targetAquired(){
		this.showTargetPanel ();
	}

	public void targetLost(){
		this.hideTargetPanel ();
	}*/

	public void updateUI(){
		this.updatePartyPanel ();
		this.updateTargetPanel ();
	}

	void Update () {
		this.updateUI ();
		this.royPanel ();
	}

	/**** SETTERS Y GETTERS ****/

	public GameManager GameManager {
		get {
			return this.gameManager;
		}
		set {
			gameManager = value;
		}
	}

	public UIConsole Console {
		get {
			return this.console;
		}
		set {
			console = value;
		}
	}



}
