﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class ResourceRarity : Definition {

	private float baseCostMultiplier;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public override string ToString ()
	{
		string result = "Rarity : " + Environment.NewLine
		                + "Id : " + this.id + Environment.NewLine
		                + "Name : " + this.name + Environment.NewLine
		                + "Description : " + this.description + Environment.NewLine;
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public float BaseCostMultiplier {
		get {
			return this.baseCostMultiplier;
		}
		set {
			baseCostMultiplier = value;
		}
	}

}