﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class CharacterConditionDef : Definition {

	int baseMinValue;
	AttributeFormula attributeFormula;
	///Considerar barColor.
	/// 
	//Ver como implementar buffs segun thresholds de value.
	//private Dictionary<int,BuffDef> thresholdBuffs;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public override string ToString(){
		return "CharacterConditionDef (" + this.key + ") : " + Environment.NewLine
			+ "ID : " + this.id + Environment.NewLine
			+ "Name : " + this.name + Environment.NewLine
			+ "Description : " + this.description + Environment.NewLine
			+ "Attribute Formula : " + this.attributeFormula.ToString() + Environment.NewLine + Environment.NewLine;
	}

	/**** SETTERS Y GETTERS ****/

	public int BaseMinValue {
		get {
			return this.baseMinValue;
		}
		set {
			baseMinValue = value;
		}
	}

	public AttributeFormula AttributeFormula {
		get {
			return this.attributeFormula;
		}
		set {
			attributeFormula = value;
		}
	}

}


