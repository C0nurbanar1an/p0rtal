﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class Skills {

	private List<Skill> skillList;
	bool complete;

	/****   CONSTRUCTORES   ****/

	public Skills(bool complete,DefinitionManager definitions){
		this.skillList = new List<Skill> ();
		this.complete = complete;
		if (complete) {
			this.addAllDefinedSkills (definitions);
		}
		this.reset ();
	}

	public Skills() : this(false,null){

	}

	/****       METODOS     ****/

	private void addAllDefinedSkills(DefinitionManager definitions){

		foreach (SkillDef skd in definitions.SkillDefinitions){
			this.addSkill (skd);
		}

	}

	public void addSkill(SkillDef skillDef){
		Skill foundSkill = this.getSkillById (skillDef.Id);
		if (foundSkill == null) {
			foundSkill = new Skill (skillDef);
			this.skillList.Add (foundSkill);	
		}
	}

	public void addSkill(Skill skill){
		Skill foundskill = getSkillById (skill.SkillDef.Id);
		if (foundskill != null){
			foundskill.addValue (skill.Value);
		} else {
			foundskill = new Skill (skill);
			this.skillList.Add (foundskill);
		}
	}

	public void addSkills(Skills skills){
		foreach (Skill skill in skills.skillList){
			this.addSkill (skill);
		}
	}

	public Skill getSkillByKey(string key){
		foreach (Skill skill in this.skillList){
			if (skill.SkillDef.Key == key){
				return skill;
			}
		}
		return null;
	}

	public Skill getSkillById(int id){
		foreach (Skill skill in this.skillList){
			if (skill.SkillDef.Id== id){
				return skill;
			}
		}
		return null;
	}

	public Skill getSkill(SkillDef skd){
		foreach (Skill sk in this.skillList) {
			if (sk.SkillDef.Id == skd.Id) {
				return sk;
			}
		}
		return null;
	}

	public void reset(){
		foreach (Skill skill in this.skillList){
			skill.reset ();
		}
	}

	public void clear(){
		this.skillList.Clear();
	}

	public string getSkillList(){
		string result = "";
		foreach (Skill sk in this.skillList) {
			result += sk.ToShortString () + Environment.NewLine;
		}
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public List<Skill> SkillList {
		get {
			return this.skillList;
		}
		set {
			skillList = value;
		}
	}

	public bool Complete {
		get {
			return this.complete;
		}
		set {
			complete = value;
		}
	}

}
