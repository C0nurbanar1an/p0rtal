﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource {

	ResourceDef resourceDef;
	int value;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public int getCraftCost(){
		return (int) this.resourceDef.Rarity.BaseCostMultiplier * this.value;
	}

	/**** SETTERS Y GETTERS ****/

	public ResourceDef ResourceDef {
		get {
			return this.resourceDef;
		}
		set {
			resourceDef = value;
		}
	}

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

}
