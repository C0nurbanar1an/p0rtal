﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageType : Definition {

	/*
	public enum DamageKey {
		BALLISTIC,
		FIRE,
		COLD,
		LASER,
		PLASMA
	}
	*/

	private const float DEFAULT_FABRIC_DAMAGE_MULTIPLIER = 1.0f;

	private Dictionary <Fabric, float> fabricDamageMultipliers;

	/****   CONSTRUCTORES   ****/

	public DamageType (){
		this.fabricDamageMultipliers = new Dictionary <Fabric, float> ();
	}

	/****       METODOS     ****/

	public float getFabricDamageMultiplier(string key){
		foreach (KeyValuePair <Fabric, float> pair in this.fabricDamageMultipliers){
			if (pair.Key.Key == key){
				return pair.Value;
			}
		}
		return DEFAULT_FABRIC_DAMAGE_MULTIPLIER;	
	}

	public float getFabricDamageMultiplier(Fabric fabric){

		foreach (KeyValuePair <Fabric, float> pair in this.fabricDamageMultipliers){
			if (pair.Key.Id == fabric.Id){
				return pair.Value;
			}
		}
		return DEFAULT_FABRIC_DAMAGE_MULTIPLIER;	

	}

	/**** SETTERS Y GETTERS ****/

	public Dictionary<Fabric, float> FabricDamageMultipliers {
		get {
			return this.fabricDamageMultipliers;
		}
		set {
			this.fabricDamageMultipliers = value;
		}
	}

}
