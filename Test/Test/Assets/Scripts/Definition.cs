﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.CompilerServices;

public class Definition {

	protected int id;
	protected string key;
	protected string name;
	protected string description;
	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public override string ToString(){

		return "Id : " + this.id + Environment.NewLine
		+ "Key : " + this.key + Environment.NewLine
		+ "Name : " + this.name + Environment.NewLine
		+ "Description : " + this.description + Environment.NewLine;

	}

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Key {
		get {
			return this.key;
		}
		set {
			key = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

}
