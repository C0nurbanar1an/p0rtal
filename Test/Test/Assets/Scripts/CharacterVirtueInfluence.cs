﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterVirtueInfluence  {

	public enum VirtueStatusEnum {
		BASE, 
		EXTRA,
		FINAL,
		NULL
	}

	protected VirtueStatusEnum virtueStatus;

	public virtual float getCharacterInfluence(Character character){
		return 0; //Nunca deberia pasar.
	}

	public VirtueStatusEnum VirtueStatus {
		get {
			return this.virtueStatus;
		}
		set {
			virtueStatus = value;
		}
	}

}
