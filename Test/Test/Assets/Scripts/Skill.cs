﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class Skill {

	private SkillDef skillDef;
	private int value;
	private int skillExp;
	private int skillLvl;

	/****   CONSTRUCTORES   ****/


	//Copy Constructor
	public Skill(Skill skill){
		this.skillDef = skill.SkillDef;
		this.value = skill.Value;
		this.skillExp = skill.SkillExp;
		this.skillLvl = skill.SkillLvl;
	}

	public Skill(SkillDef skillDef){
		this.skillDef = skillDef;
		this.value = 0;
		this.skillExp = 0;
		this.skillLvl = 0;
	}

	/****       METODOS     ****/

	public void setValueByFormula (Character character){
		this.value = this.skillDef.AttributeFormula.calculateValue (character.CharacterAttributes.FinalAttributes);
	}

	public void reset(){
		this.resetValue ();
		this.resetExp ();
	}

	public void resetValue(){
		//this.value = this.skillDef.BaseValue;
		this.value = 0;
	}

	public void resetExp(){
		this.skillExp = 0;
		this.updateLvl ();
	}

	public void addExp(int exp){

		this.skillExp += exp;
		this.updateLvl ();
	}

	public void substractExp(int exp){
		if (this.skillExp != 0){
			this.skillExp += exp;
			if (exp < 0)
				this.skillExp = 0;
		}
		this.updateLvl ();

	}

	public void addValue(int value){
		this.value += value;
	}

	public void updateLvl(){
		this.skillLvl = this.calculateSkillLvl();
	}

	private int calculateSkillLvl(){
		return this.skillDef.expToLvl (this.skillExp);
	}

	public string ToShortString(){
		return this.skillDef.Key + " : " + this.value;
	}

	public void addSkill(Skill skill){
		if (skill != null)
			this.value += skill.Value;
	}

	/**** SETTERS Y GETTERS ****/

	public SkillDef SkillDef {
		get {
			return this.skillDef;
		}
		set {
			skillDef = value;
		}
	}

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

	public int SkillExp {
		get {
			return this.skillExp;
		}
		set {
			skillExp = value;
		}
	}

	public int SkillLvl {
		get {
			return this.skillLvl;
		}
		set {
			skillLvl = value;
		}
	}

}

