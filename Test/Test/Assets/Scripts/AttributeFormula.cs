﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System;


/**** Ver de generalizar la clase de Formula****/
public class AttributeFormula  {

	int baseValue;
	List<AttributeInfluence> attributeInfluences;

	/****   CONSTRUCTORES   ****/

	public AttributeFormula(){
		this.attributeInfluences = new List<AttributeInfluence> ();
	}


	/****       METODOS     ****/
		
	public bool hasAttribute(AttributeDef attributeDef){
		foreach (AttributeInfluence ai in this.attributeInfluences){
			if (ai.AttributeDef.Id == attributeDef.Id) {
				return true;
			}
		}
		return false;
	}

	public int calculateValue(Attributes attributes){
		Attribute attr;
		int value = 0;
		foreach (AttributeInfluence ai in this.attributeInfluences) {
			attr = attributes.getAttributeById (ai.AttributeDef.Id);
			if (attr != null){
				value += (int) ai.Multiplier * attr.Value;
			}
		
		}
		value += baseValue;
		return value;

	}

	public override string ToString(){
		string result = "" + Environment.NewLine;
		foreach (AttributeInfluence ai in this.attributeInfluences){
			result += "    " + ai.AttributeDef.Key
				+ " X " + ai.Multiplier + Environment.NewLine;
		}
		return result;
	}


	/**** SETTERS Y GETTERS ****/

	 
	public int BaseValue {
		get {
			return this.baseValue;
		}
		set {
			baseValue = value;
		}
	}

	public List<AttributeInfluence> AttributeInfluences {
		get {
			return this.attributeInfluences;
		}
		set {
			attributeInfluences = value;
		}
	}

}
