﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;	

public class CharacterMB : MonoBehaviour {

	private Character character;
	private GameObject cameraGO;
	private Outline outline;
	private Outline outline2;
	private GameManager gameManager;
	private LayerMask charactersLayer;
	private UI ui;
	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	// Use this for initialization
	void Start () {
		this.cameraGO = GameObject.Find ("Main Camera");
		this.charactersLayer = LayerMask.GetMask ("Character"); 
		//Outline.
		Outline [] outlines = gameObject.GetComponents<Outline>();
		this.outline = outlines[0];
		this.outline.enabled = false;
		this.outline2 = outlines [1];
		this.outline2.enabled = false;
		//this.view = GameObject.Find ("View").GetComponent<View> ();
		this.gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();

	}
	
	// Update is called once per frame
	void Update () {
		//transform.LookAt(Camera.main.transform.position - transform.position, -Vector3.up);
		transform.rotation = Quaternion.LookRotation(transform.position - this.cameraGO.transform.position);
		this.checkIfClicked ();
	}

	private void checkIfClicked(){
		RaycastHit hitInfo = new RaycastHit ();
		if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hitInfo, 9999, charactersLayer.value)) {
			if (hitInfo.collider.gameObject == this.gameObject) {
				this.checkClicks ();
			}
		}
	}

	private void checkClicks(){

		if (Input.GetMouseButtonDown(0)){
			this.gameManager.CurrentController.onCharacterClicked(this,0);
			//Debug.Log("Pressed left click on " + this.character.Biography.Name);
			return;
		}
			
		if (Input.GetMouseButtonDown(1)){
			this.gameManager.CurrentController.onCharacterClicked(this,1);
			//Debug.Log("Pressed right click on " + this.character.Biography.Name);
			return;
		}
			
		if (Input.GetMouseButtonDown(2)){
			this.gameManager.CurrentController.onCharacterClicked(this,2);
			//Debug.Log("Pressed middle click on " + this.character.Biography.Name);
			return;
		}

	}

	void OnMouseEnter() {
		this.gameManager.CurrentController.characterOnMouseEnter (this);
		//this.outline.enabled = true;
	}

	void OnMouseExit() {
		this.gameManager.CurrentController.characterOnMouseExit (this);
		this.disableOutline ();
	}

	public void enableOutline(int color){
		this.outline.enabled = true;
		this.outline.color = color;
	}

	public void disableOutline(){
		this.outline.enabled = false;
	}

	public void enableOutline2(int color){
		this.outline2.enabled = true;
		this.outline2.color = color;
	}

	public void disableOutline2(){
		this.outline2.enabled = false;
	}

	public void toggleOutline(){
		this.outline.enabled = !this.outline.enabled;
	}

	public void setCharacter(Character character){
		if (this.character == null)
			this.character = character;
		else {
			//Manejar errores o tomar una decision.
		}
		if (character.CharacterMB == null) {
			character.CharacterMB = this;
		}
	}

	public void floatDamageReceived(int damage){
		UI.createFloatingText (damage.ToString (), this.transform);
	}

	/**** SETTERS Y GETTERS ****/

	public Character Character {
		get {
			return this.character;
		}
		set {
			character = value;
		}
	}

	public GameObject CameraGO {
		get {
			return this.cameraGO;
		}
		set {
			cameraGO = value;
		}
	}

	public Outline Outline {
		get {
			return this.outline;
		}
		set {
			outline = value;
		}
	}

	public GameManager GameManager {
		get {
			return this.gameManager;
		}
		set {
			gameManager = value;
		}
	}

	public LayerMask CharactersLayer {
		get {
			return this.charactersLayer;
		}
		set {
			charactersLayer = value;
		}
	}
		
}
