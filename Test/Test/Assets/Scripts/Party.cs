﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Party {

	public const int MAX_PARTY_MEMBERS = 6;

	private Civilization civilization;
	private List<Character> characters;


	/****   CONSTRUCTORES   ****/

	public Party(){
		this.characters = new List<Character> ();
	}	

	/****       METODOS     ****/

	public Character getCharacterAtIndex(int index){
		return this.characters [index];
	}

	public int getCharacterIndex(Character character){
		return this.characters.IndexOf (character);
	}

	private void addCharacter(Character character){
		this.characters.Add (character);
		character.Party = this;

	}

	public bool tryToAddCharacter(Character character){
		if (this.characters.Count < MAX_PARTY_MEMBERS) {
			this.addCharacter (character);
			return true;
		} else {
			//Notify GUI.
			return false;
		}
	}

	public bool isAnyoneAlive(){
		foreach (Character c in this.characters){
			if (c.isAlive()){
				return true;
				}
		}
		return false;
	}

	public bool areAllDead(){
		return (!this.isAnyoneAlive());
	}

	public bool isPlayerOwned(){
		return false;
		//TODO : comunicarse con GameManager y consultar.
	}

	/**** SETTERS Y GETTERS ****/

	public List<Character> Characters {
		get {
			return this.characters;
		}
		set {
			characters = value;
		}
	}

}
