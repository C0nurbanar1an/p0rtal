﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class CharacterEffectFormula {

	CharacterInfluences casterInfluences;
	CharacterInfluences targetInfluences;
	int fixedValue;

	/****   CONSTRUCTORES   ****/

	public CharacterEffectFormula (){
		this.casterInfluences = new CharacterInfluences ();
		this.targetInfluences = new CharacterInfluences ();
	}

	/****       METODOS     ****/

	public int getFormulaValue(Character caster, Character target){

		int value = 0;

		if (caster != null) {
			value += casterInfluences.getCharacterInfluencesValue (caster);
		}

		if (target != null) {
			value += targetInfluences.getCharacterInfluencesValue (target);
		}	
		value += fixedValue;
		//Debug.LogWarning ("Total Value : " + value);
		return value;

	}

	public override string ToString ()
	{
		string result = "Formula : " + Environment.NewLine;
		result += "Fixed Value : " + this.fixedValue + Environment.NewLine;
		if (this.casterInfluences.hasInfluences ()) {
			result += "Caster Influences : " + Environment.NewLine;
			result += casterInfluences.ToString () + Environment.NewLine;
		}
		if (this.targetInfluences.hasInfluences ()) {
			result += "Target Influences : " + Environment.NewLine;
			result += targetInfluences.ToString () + Environment.NewLine;
		}
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public CharacterInfluences CasterInfluences {
		get {
			return this.casterInfluences;
		}
		set {
			casterInfluences = value;
		}
	}

	public CharacterInfluences TargetInfluences {
		get {
			return this.targetInfluences;
		}
		set {
			targetInfluences = value;
		}
	}

	public int FixedValue {
		get {
			return this.fixedValue;
		}
		set {
			fixedValue = value;
		}
	}

}
