﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building {

	Settlement settlement;
	BuildingDef buildingDef;
	int lvl;
	List<Service> services;

	List<ItemRecipe> craftableItemRecipes;
	List<ItemType> repairableItemTypes;
	List<TechDef> researchableTechs;
	List<AttributeDef> trainableAttributes;
	List<BuffDef> removableBuffs;

	/****   CONSTRUCTORES   ****/

	public Building(BuildingDef buildingDef) : this(buildingDef,0){
		
	}

	public Building(BuildingDef buildingDef, int lvl){
		this.buildingDef = buildingDef;
		this.lvl = lvl;
		this.services = new List<Service> ();
		this.updateBuildingProperties ();
	}

	/****       METODOS     ****/

	public bool canBeUpgraded(){
		return (this.lvl < this.buildingDef.MaxLvl);
	}

	public Resources getUpgradeCost(){
		return this.buildingDef.ResourceCostPerLvl [this.lvl + 1];
	}

	public void upgrade(){
		this.lvl++;
		this.updateBuildingProperties ();
	}

	public bool isMaxLvl(){
		return (this.lvl == this.buildingDef.MaxLvl);
	}

	public void addService(Service service){
		this.services.Add (service);
		//character.setWorking (true);
	}

	public bool tryToAddService(Service service){
		if (this.canAddService ()) {
			this.addService (service);
			return true;
		} else {
			//Cant add service.
			//notifyGUI
			return false;
		}
	}

	public bool canAddService(){
		return (this.services.Count < this.buildingDef.MaxServicesPerLvl [this.lvl]);
	}


	private void updateBuildingProperties(){
		this.updateCraftableItemRecipes();
		this.updateResearchableTechs ();
		this.updateRepairableItemTypes ();
		this.updateTrainableAttributes ();
		this.updateRemovableBuffs ();
	}

	private void updateCraftableItemRecipes(){
		this.craftableItemRecipes.Clear ();
		for (int k = 0;k < this.lvl;k++){
			foreach (ItemRecipe ir in this.buildingDef.CraftableItemRecipesPerLvl[k]){
				this.craftableItemRecipes.Add (ir);	
			}
		}
	}

	private void updateResearchableTechs(){
		this.researchableTechs.Clear ();
		for (int k = 0;k < this.lvl;k++){
			foreach (TechDef t in this.buildingDef.ResearchableTechsPerLvl[k]){
				this.researchableTechs.Add (t);	
			}
		}
	}

	private void updateRepairableItemTypes(){
		this.repairableItemTypes.Clear ();
		for (int k = 0;k < this.lvl;k++){
			foreach (ItemType it in this.buildingDef.RepairableItemTypesPerLvl[k]){
				this.repairableItemTypes.Add (it);	
			}
		}
	}

	private void updateTrainableAttributes(){
		this.trainableAttributes.Clear ();
		for (int k = 0;k < this.lvl;k++){
			foreach (AttributeDef ad in this.buildingDef.TrainableAttributesPerLvl[k]){
				this.trainableAttributes.Add (ad);	
			}
		}
	}

	private void updateRemovableBuffs(){
		this.removableBuffs.Clear ();
		for (int k = 0;k < this.lvl;k++){
			foreach (BuffDef bd in this.buildingDef.RemovableBuffsPerLvl[k]){
				this.removableBuffs.Add (bd);	
			}
		}
	}

	public bool canCraftItems(){
		return (this.craftableItemRecipes.Count > 0);
	}

	public bool canResearchTechs(){
		return (this.researchableTechs.Count > 0);
	}

	public bool canTrainAttributes(){
		return (this.trainableAttributes.Count > 0);
	}

	public bool canRemoveBuffs(){
		return (this.removableBuffs.Count > 0);
	}

	public bool canRepairItems(){
		return (this.repairableItemTypes.Count > 0);
	}

	/**** SETTERS Y GETTERS ****/

	public Settlement Settlement {
		get {
			return this.settlement;
		}
		set {
			settlement = value;
		}
	}

	public int Lvl {
		get {
			return this.lvl;
		}
		set {
			lvl = value;
			if (lvl < 0){
				lvl = 0;
			}
			this.updateBuildingProperties ();
		}
	}

	public BuildingDef BuildingDef {
		get {
			return this.buildingDef;
		}
		set {
			buildingDef = value;
		}
	}
		
	public List<Service> Services {
		get {
			return this.services;
		}
		set {
			services = value;
		}
	}

	public List<ItemRecipe> CraftableItemRecipes {
		get {
			return this.craftableItemRecipes;
		}
		set {
			craftableItemRecipes = value;
		}
	}

	public List<ItemType> RepairableItemTypes {
		get {
			return this.repairableItemTypes;
		}
		set {
			repairableItemTypes = value;
		}
	}

	public List<TechDef> ResearchableTechs {
		get {
			return this.researchableTechs;
		}
		set {
			researchableTechs = value;
		}
	}

	public List<AttributeDef> TrainableAttributes {
		get {
			return this.trainableAttributes;
		}
		set {
			trainableAttributes = value;
		}
	}

	public List<BuffDef> RemovableBuffs {
		get {
			return this.removableBuffs;
		}
		set {
			removableBuffs = value;
		}
	}



}
