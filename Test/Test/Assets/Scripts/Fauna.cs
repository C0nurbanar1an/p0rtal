﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fauna {

	int id;
	List<FloraSpecies> species;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public List<FloraSpecies> Species {
		get {
			return this.species;
		}
		set {
			species = value;
		}
	}

}