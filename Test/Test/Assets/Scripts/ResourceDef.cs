﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class ResourceDef : Definition{

	Sprite sprite;
	ResourceRarity rarity;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public override string ToString ()
	{
		string result = "Resource : " + Environment.NewLine
		                + "Id : " + this.id + Environment.NewLine
		                + "Name : " + this.name + Environment.NewLine
		                + "Description : " + this.description + Environment.NewLine
		                + "Rarity : " + rarity.ToString () + Environment.NewLine;
		if (this.sprite != null) 
			result += "Sprite = " + this.sprite.name + Environment.NewLine
				+ Environment.NewLine + Environment.NewLine;
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			sprite = value;
		}
	}

	public ResourceRarity Rarity {
		get {
			return this.rarity;
		}
		set {
			rarity = value;
		}
	}

}
