﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class HealCharacterEffect : CharacterEffect {

	/****   CONSTRUCTORES   ****/

	public HealCharacterEffect (){
		this.type = EffectType.HEAL_CHARACTER;
	}

	/****       METODOS     ****/

	public override void apply(Character caster, Character target){

		target.receiveHealing (this.createHealing (caster, target));	

	}


	public Healing createHealing(Character caster,
								 Character target){
		Healing healing = new Healing ();
		healing.Value = this.getHealingValue(caster,target);
		healing.Healer = caster;
		return healing;
	}

	private int getHealingValue(Character caster,
							    Character target){

		return this.formula.getFormulaValue(caster,target);

	}

	public override string ToString (){

		string result = "Heal Character Effect : " + Environment.NewLine;
		result += this.formula.ToString ();

		return result;
	}

	/**** SETTERS Y GETTERS ****/



}
