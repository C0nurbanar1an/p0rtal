﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System;

public class InfluenceFormula  {

	private int baseValue;
	private List<AttributeInfluence> attributeInfluences;
	private List<SkillInfluence> skillInfluences;


	/****   CONSTRUCTORES   ****/

	public InfluenceFormula(){
		this.attributeInfluences = new List<AttributeInfluence> ();
		this.skillInfluences = new List<SkillInfluence> ();
	}


	/****       METODOS     ****/

	public float calculateValue(Character character){

		float value = 0.0f;

		foreach (AttributeInfluence ai in this.attributeInfluences) {
			value += ai.getCharacterInfluence (character);
		}

		foreach (SkillInfluence ski in this.skillInfluences) {
			value += ski.getCharacterInfluence (character);
		}

		return value;

	}

	public override string ToString(){
		string result = "Attribute Influences : " + Environment.NewLine;

		foreach (AttributeInfluence ai in this.attributeInfluences){
			result += "    " + ai.AttributeDef.Key
				+ " X " + ai.Multiplier + Environment.NewLine;
		}

		result += "Skill Influences : " + Environment.NewLine;

		foreach (SkillInfluence ski in this.skillInfluences){
			result += "    " + ski.SkillDef.Key
				+ " X " + ski.Multiplier + Environment.NewLine;
		}

		return result;
	}

}
