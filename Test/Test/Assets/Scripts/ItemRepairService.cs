﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRepairService : Service {

	private Item item;


	/****   CONSTRUCTORES   ****/

	public ItemRepairService(Item item){
		this.item = item;
		this.minWorkers = 1;
	}

	/****       METODOS     ****/

	protected override void onCompleted (){
		//Notify
	}

	protected override void updateProgress(){
		this.addProgressToService (this.ProgressPerMinutes(1));
	}

	public int ProgressPerMinute (){

		int total = 0;
		foreach (Character c in this.workers){
			total += c.CharacterSkills.FinalSkills.getSkillByKey ("CRF").Value;
		}
		return total;

	}

	protected override bool isCompleted(){
		return item.isWhole();
	}

	protected override void addProgress(int progress){
		this.item.addDurability (progress);
	}

	/**** SETTERS Y GETTERS ****/

	public Item Item {
		get {
			return this.item;
		}
		set {
			item = value;
		}
	}

}
