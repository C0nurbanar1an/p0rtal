﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biography {

	string name;
	string background;
	string birthplace;
	Sprite portrait;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Background {
		get {
			return this.background;
		}
		set {
			background = value;
		}
	}

	public string Birthplace {
		get {
			return this.birthplace;
		}
		set {
			birthplace = value;
		}
	}

	public Sprite Portrait {
		get {
			return this.portrait;
		}
		set {
			portrait = value;
		}
	}

}