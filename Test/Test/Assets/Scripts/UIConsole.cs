﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Schema;
using System;

public class UIConsole : MonoBehaviour {

	private GameManager gameManager;
	private Text consoleText;

	public void printMessage(string msg){
		this.consoleText.text += Environment.NewLine + msg;
	}

	void Start () {
		this.gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		this.consoleText = gameObject.GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
