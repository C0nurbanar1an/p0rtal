﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class AttributeDef : Definition {

	public const int ATTRIBUTE_MAX_LVL = 5;
	public const int BASE_TRAINING_PER_MINUTE = 1;

	private static readonly int[] lvlExpMilestonesArray = 	{500,1500,3000,5000,7500 };
	//Clase canónica de los atributos.
	//Va a ver una instancia por tipo de Atributo.

	//Attribute Lvl.
	private List<int> lvlExpMilestones;
	private int maxLvl = ATTRIBUTE_MAX_LVL;
	private int baseValue;

	/****   CONSTRUCTORES   ****/

	public AttributeDef(){
		this.lvlExpMilestones = new List<int> (AttributeDef.lvlExpMilestonesArray);
	}
		
	/****      METODOS      ****/

	public int expToLvl(int exp){
		int i;
		for (i = this.maxLvl - 1;i >= 0 ; i--){
			if (exp >= this.lvlExpMilestones[i]){
				return i + 1;
			}
		}
		return 0;
	}

	public override string ToString(){
		return "AttributeDef (" + this.key + ") : " + Environment.NewLine
			+  "ID : " + this.id + Environment.NewLine
			+ "Name : " + this.name + Environment.NewLine
			+ "Description : " + this.description + Environment.NewLine;
	}

	/**** SETTERS Y GETTERS ****/

	public List<int> LvlExpMilestones {
		get {
			return this.lvlExpMilestones;
		}
		set {
			lvlExpMilestones = value;
		}
	}

	public int BaseValue {
		get {
			return this.baseValue;
		}
		set {
			baseValue = value;
		}
	}



}
