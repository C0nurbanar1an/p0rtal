﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldUI : MonoBehaviour {

	private Party currentParty;
	public Character targetCharacter;
	private int currentPartySlot;


	public void setCurrentPartySlot(int slot){
		if (slot < 0 || slot > Party.MAX_PARTY_MEMBERS) {
			Debug.LogError ("Slot " + slot + " is out of bounds!");
		} else {
			this.currentPartySlot = slot;
			//Do stuff with UI.
		}
	}

	public void setCurrentCharacter (Character character){
	
		int index = this.currentParty.getCharacterIndex(character);
		if (index > -1) {
			this.setCurrentPartySlot (index);
		} else {
			Debug.LogError ("Character " + character.Biography.Name + "not found on Party");
		}

	}

	public void setTargetCharacter(Character target){
		this.targetCharacter = target;
		//Update UI data.
	}
		
	public void Update(){
		//Update UI Stuff.
		if (this.targetCharacter != null) {
			//Fill Target UI data.
			//Fill buffs.
			//Fill Conditions.
			//Fill Descriptions.
		} else {
			//Fill Null data or hide target window.
		}

		if (this.currentPartySlot > -1) {
			//Fill Party UI Data.
			//Fill Abilities.
			//Fill Conditions.
			//Activate proper tab and hide all others.
		} else {
			//Fill with null Data, clear or hide.
		}

	}

}