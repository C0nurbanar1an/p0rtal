﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCondition  {

	CharacterConditionDef characterConditionDef;
	CharacterConditions characterConditions;
	int value;
	int maxValue;
	int minValue;

	/****   CONSTRUCTORES   ****/

	public CharacterCondition(){

	}

	public CharacterCondition (CharacterConditionDef ccd) : this(ccd,null){

	}

	public CharacterCondition(CharacterConditionDef ccd, CharacterConditions characterConditions){

		this.characterConditionDef = ccd;
		this.characterConditions = characterConditions;
		this.updateBoundaries ();
		this.value = this.maxValue;

	}

	/****       METODOS     ****/

	public void refill(){
		this.value = this.maxValue;
	}

	public void alterValue(int value){
		if (value > 0)
			this.addValue (value);
		if (value < 0)
			this.substractValue (-value);
	}

	public void addValue(int value){
		this.value += value;
		if (this.value > this.MaxValue){
			this.value = this.MaxValue;
			this.reachedMaxValue ();
		}
		//Check Status change for buffs.
	}

	private void reachedMaxValue(){
		//TODO : Ver como implementar esto.
	}

	public void substractValue(int value){
		this.value -= value;
		if (this.value < this.MinValue){
			this.value = this.MinValue;
			this.reachedMinValue ();
		}
		//Check Status change for buffs.
	}

	private void reachedMinValue(){
		//TODO : Ver como implementar esto.
	}

	public void updateBoundaries(){
		int prevMaxValue = this.maxValue;
		int prevMinValue = this.minValue;

		this.minValue = this.characterConditionDef.BaseMinValue;
		this.maxValue = this.characterConditionDef.AttributeFormula.calculateValue(
			this.characterConditions.Character.CharacterAttributes.FinalAttributes);

		if (this.maxValue < this.Value) {
			this.value = this.maxValue;
		} else {
			int deltaMaxValue = this.maxValue - prevMaxValue;
			if (deltaMaxValue > 0) {
				this.value += deltaMaxValue;
			}
		}

	}

	/**** SETTERS Y GETTERS ****/

	public CharacterConditionDef CharacterConditionDef {
		get {
			return this.characterConditionDef;
		}
		set {
			characterConditionDef = value;
		}
	}

	public CharacterConditions CharacterConditions {
		get {
			return this.characterConditions;
		}
		set {
			characterConditions = value;
		}
	}

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

	public int MaxValue {
		get {
			return this.maxValue;
		}
		set {
			maxValue = value;
		}
	}

	public int MinValue {
		get {
			return this.minValue;
		}
		set {
			minValue = value;
		}
	}

}
