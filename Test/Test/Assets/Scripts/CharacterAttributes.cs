﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterAttributes {

	private Character character;
	private Attributes baseAttributes; //Complete
	private Attributes extraAttributes; //Non-complete
	private Attributes finalAttributes; //Complete
	//Implementar esto si hacia problemas de performance.
	//Seria un diccionario compuesto de <GUID,Attributes>.
	//private Dictionary<string,Attributes> buffAttributes; //All non-complete.

	//Aplicar los atributos y skills de los buffs por orden de recepcion y no rompamos mas la japi.
	/****   CONSTRUCTORES   ****/



	/****       METODOS     ****/

	public CharacterAttributes (Character character,DefinitionManager definitions){
		this.character = character;
		this.baseAttributes = new Attributes (true,definitions);
		this.extraAttributes = new Attributes (false,definitions);
		this.finalAttributes = new Attributes (true,definitions);
		this.initialize ();
	}

	public void initialize(){
		this.initializeAttributes();
	}

	public void initializeAttributes(){
		//Actualizamos los Atributos Base.
		this.updateBaseAttributes ();
		this.clearExtraAttributes ();
		this.updateFinalAttributes ();
	}

	public void addExtraAttribute(Attribute attr){
		this.extraAttributes.addAttribute (attr);
		this.finalAttributes.addAttribute(attr);
		//this.updateFinalAttribute(attr.AttributeDef);
		//this.updateFinalAttributes ();
	}

	private void clearExtraAttributes(){
		this.extraAttributes.clear ();
	}

	private void updateBaseAttributes(){

		//Este metodo no deberia llamarse solo.
		//Tiene que ser llamado por updateAttributes.

		Attributes raceAttributes, traitsAttributes, geneticsAttributes;
		raceAttributes = this.character.getRaceAttributeModifiers ();
		traitsAttributes = this.character.getTraitsAttributeModifiers ();
		geneticsAttributes = this.character.GeneticsAttributes;

		this.baseAttributes.resetToDefaults ();
		this.baseAttributes.addAttributes (raceAttributes);
		this.baseAttributes.addAttributes (traitsAttributes);
		this.baseAttributes.addAttributes (geneticsAttributes);

	}

	private void updateExtraAttributes(){
		this.extraAttributes.clear ();
		//this.buffAttributes.clear ();



		//this.updateFinalAttributes ();

		//Falta proceso.
	}

	public void updateFinalAttribute(AttributeDef attributeDef){
	
		Attribute finalAttribute = this.finalAttributes.getAttribute (attributeDef);
		Attribute baseAttribute = this.baseAttributes.getAttribute (attributeDef);
		Attribute extraAttribute = this.extraAttributes.getAttribute (attributeDef);

		finalAttribute.reset ();

		if (baseAttribute != null) {
			finalAttribute.addValue (baseAttribute.Value);
		}

		if (extraAttribute != null) {
			finalAttribute.addValue(extraAttribute.Value);
		}
			
	}

	public void updateFinalAttributes (){
		this.finalAttributes.reset ();
		this.finalAttributes.addAttributes (this.baseAttributes);
		this.finalAttributes.addAttributes (this.extraAttributes);
	}

	public void resetAttributes(){
		this.baseAttributes.reset ();
		this.extraAttributes.clear ();
		this.finalAttributes.reset ();
	}

	public string getFinalAttributeList(){
		return this.finalAttributes.getAttributeListString ();
	}

	/**** SETTERS Y GETTERS ****/


	public Character Character {
		get {
			return this.character;
		}
		set {
			character = value;
		}
	}

	public Attributes BaseAttributes {
		get {
			return this.baseAttributes;
		}
		set {
			baseAttributes = value;
		}
	}

	public Attributes ExtraAttributes {
		get {
			return this.extraAttributes;
		}
		set {
			extraAttributes = value;
		}
	}

	public Attributes FinalAttributes {
		get {
			return this.finalAttributes;
		}
		set {
			finalAttributes = value;
		}
	}

}
