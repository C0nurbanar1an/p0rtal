﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ConditionBar : MonoBehaviour {

	public RectTransform healthTransform;
	public Vector2 size;
	public Image image;
	public Color32 color;
	public float width;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image> ();
		healthTransform = GetComponent<RectTransform> ();
		size = healthTransform.sizeDelta;
		color = image.color;
		width = healthTransform.rect.width;
	}
		
	/*public void showValue(int currentValue){
		size += new Vector2 (currentValue, 0);
		healthTransform.sizeDelta = size;
		width = healthTransform.rect.width;
		if(width > 50){
			color = new Color32 ((byte)(255 - (width * 5.1)) , 255, 0, 255);	
		}else{
			color = new Color32 (255, (byte)(width * 5.1), 0, 255);
		}
		image.color = color;
	}*/

	public void showValue(CharacterCondition condition){
		int value = condition.Value * 100 / condition.MaxValue;
		size = new Vector2 (value, 10);
		healthTransform.sizeDelta = size;
		width = healthTransform.rect.width;
		if(width > 50){
			color = new Color32 ((byte)(255 - (width * 5.1)) , 255, 0, 255);	
		}else{
			color = new Color32 (255, (byte)(width * 5.1), 0, 255);
		}
		image.color = color;
	}
}
