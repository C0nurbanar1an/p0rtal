﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class AlterCharacterSkillEffect : CharacterEffect {

	SkillDef skillDef;

	/****   CONSTRUCTORES   ****/

	public AlterCharacterSkillEffect(){
		this.type = EffectType.ALTER_SKILL;
	}

	/****       METODOS     ****/

	public override void apply(Character caster, Character target){
		Skill skillToAdd = getSkillToAdd (caster, target);
		target.addExtraSkill (skillToAdd);	

		//Debug.Log ("AlterAttribute applied to " + target.Biography.Name);
	}

	public Skill getSkillToAdd(Character caster, Character target){
		Skill sk = new Skill (this.skillDef);
		sk.Value = this.getFormulaValue (caster, target);
		return sk;
	}

	public int getDeltaValue(Character caster, Character target){

		return this.formula.getFormulaValue(caster,target);

	}

	public override string ToString(){

		string result = "Alter Skill Effect : " + Environment.NewLine;
		result += "Skill : " + this.skillDef.Key + Environment.NewLine;
		result += this.formula.ToString() + Environment.NewLine;
		return result;
	}
	/**** SETTERS Y GETTERS ****/

	public SkillDef SkillDef {
		get {
			return this.skillDef;
		}
		set {
			skillDef = value;
		}
	}

}
