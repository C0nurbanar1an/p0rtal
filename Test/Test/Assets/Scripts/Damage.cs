﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage {

	DamageType damageType;
	int value;
	Character damageDealer;

	/****   CONSTRUCTORES   ****/

	public Damage(){

	}

	public Damage(DamageType damageType,
			      int value,
		          Character damageDealer){
		this.damageType = damageType;
		this.value = value;
		this.damageDealer = damageDealer;
	}

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public DamageType DamageType {
		get {
			return this.damageType;
		}
		set {
			damageType = value;
		}
	}

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

	public Character DamageDealer {
		get {
			return this.damageDealer;
		}
		set {
			damageDealer = value;
		}
	}

}
