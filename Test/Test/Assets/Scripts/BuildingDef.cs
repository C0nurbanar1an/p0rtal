﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDef : Definition {

	int maxLvl; 
	List<int> maxServicesPerLvl;
	List<int> buildingTimePerLvl;
	List<Sprite> spritePerLvl;
	List<Resources> resourceCostPerLvl;

	List<List<ItemRecipe>> craftableItemRecipesPerLvl;
	List<List<ItemType>> repairableItemTypesPerLvl;
	List<List<TechDef>> researchableTechsPerLvl;
	List<List<AttributeDef>> trainableAttributesPerLvl;
	List<List<BuffDef>> removableBuffsPerLvl;



	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public Resources getBuildCost(){
		return this.ResourceCostPerLvl [0];
	}

	/**** SETTERS Y GETTERS ****/

	public int MaxLvl {
		get {
			return this.maxLvl;
		}
		set {
			maxLvl = value;
		}
	}

	public List<int> MaxServicesPerLvl {
		get {
			return this.maxServicesPerLvl;
		}
		set {
			maxServicesPerLvl = value;
		}
	}

	public List<int> BuildingTimePerLvl {
		get {
			return this.buildingTimePerLvl;
		}
		set {
			buildingTimePerLvl = value;
		}
	}

	public List<Sprite> SpritePerLvl {
		get {
			return this.spritePerLvl;
		}
		set {
			spritePerLvl = value;
		}
	}

	public List<Resources> ResourceCostPerLvl {
		get {
			return this.resourceCostPerLvl;
		}
		set {
			resourceCostPerLvl = value;
		}
	}

	public List<List<ItemRecipe>> CraftableItemRecipesPerLvl {
		get {
			return this.craftableItemRecipesPerLvl;
		}
		set {
			craftableItemRecipesPerLvl = value;
		}
	}

	public List<List<ItemType>> RepairableItemTypesPerLvl {
		get {
			return this.repairableItemTypesPerLvl;
		}
		set {
			repairableItemTypesPerLvl = value;
		}
	}

	public List<List<TechDef>> ResearchableTechsPerLvl {
		get {
			return this.researchableTechsPerLvl;
		}
		set {
			researchableTechsPerLvl = value;
		}
	}

	public List<List<AttributeDef>> TrainableAttributesPerLvl {
		get {
			return this.trainableAttributesPerLvl;
		}
		set {
			trainableAttributesPerLvl = value;
		}
	}

	public List<List<BuffDef>> RemovableBuffsPerLvl {
		get {
			return this.removableBuffsPerLvl;
		}
		set {
			removableBuffsPerLvl = value;
		}
	}


}
