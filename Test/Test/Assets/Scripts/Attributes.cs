﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class Attributes  {

	List<Attribute> attributeList;
	bool complete;
	/****   CONSTRUCTORES   ****/

	public Attributes(bool complete, DefinitionManager definitions){
		this.attributeList = new List<Attribute> ();
		this.complete = complete;
		if (complete) {
			//return null;
			this.addAllDefinedAttributes (definitions);
		}
		this.reset ();
	}

	public Attributes () : this (false, null){

	}

	/****      METODOS      ****/

	private void addAllDefinedAttributes(DefinitionManager definitions){

		foreach (AttributeDef ad in definitions.AttributeDefinitions){
			this.addAttribute (ad);
		}

	}

	public void addAttribute(AttributeDef attributeDef){
		Attribute foundAttr = getAttributeByKey (attributeDef.Key);
		if (foundAttr == null) {
			this.addAttribute (new Attribute (attributeDef));
		} else {
			Debug.LogError ("Tried to add Attribute " + attributeDef.Name + " when it already exists in Attributes!");
		}
	}

	public void addAttribute(Attribute attr){
		Attribute foundAttr = getAttributeByKey (attr.AttributeDef.Key);
		if (foundAttr != null){
			foundAttr.addValue (attr.Value);
		} else {
			foundAttr = new Attribute (attr);
			this.attributeList.Add (foundAttr);
		}
	}

	public void addAttributes(Attributes attrs){
		foreach (Attribute attr in attrs.attributeList){
			this.addAttribute (attr);
		}
	}

	public Attribute getAttributeByKey(string key){
		foreach (Attribute attr in this.attributeList){
			if (attr.AttributeDef.Key == key){
				return attr;
			}
		}
		return null;
	}

	public Attribute getAttributeById(int id){
		foreach (Attribute attr in this.attributeList){
			if (attr.AttributeDef.Id == id){
				return attr;
			}
		}
		return null;
	}

	public Attribute getAttribute(AttributeDef attributeDef){
		return this.getAttributeById (attributeDef.Id);
	}

	public void reset(){
		foreach (Attribute attr in this.attributeList){
			attr.reset ();
		}
	}

	public void resetToDefaults(){
		foreach (Attribute attr in this.attributeList){
			attr.resetValueToDefault ();
		}
	}

	public void clear(){
		this.attributeList.Clear ();
	}

	public string getAttributeListString(){
		string result = "";
		foreach (Attribute attribute in this.attributeList) {
			result += attribute.toShortString () + Environment.NewLine;
		}
		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public List<Attribute> AttributeList {
		get {
			return this.attributeList;
		}
		set {
			attributeList = value;
		}
	}

	public bool Complete {
		get {
			return this.complete;
		}
	}


}
