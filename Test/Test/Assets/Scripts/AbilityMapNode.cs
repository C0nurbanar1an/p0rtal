﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityMapNode  {

	Ability ability;
	AbilityMapNode requiredAbilityMapNodes;
	bool learnt;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public void toggleLearnt(){
		this.learnt = !this.learnt;
	}

	/**** SETTERS Y GETTERS ****/


	public Ability Ability {
		get {
			return this.ability;
		}
		set {
			ability = value;
		}
	}

	public AbilityMapNode RequiredAbilityMapNodes {
		get {
			return this.requiredAbilityMapNodes;
		}
		set {
			requiredAbilityMapNodes = value;
		}
	}

	public bool Learnt {
		get {
			return this.learnt;
		}
		set {
			learnt = value;
		}
	}


}
