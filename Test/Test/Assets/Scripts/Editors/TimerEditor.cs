﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

//[CustomEditor(typeof(Timer))]
[CanEditMultipleObjects]

public class TimerEditor : Editor {

	SerializedProperty timer;
	int currentSpeedIndex;

	void OnEnable(){
		timer = serializedObject.FindProperty("timer");
	}

	public override void OnInspectorGUI(){
		serializedObject.Update();
		EditorGUILayout.PropertyField(timer	);
		serializedObject.ApplyModifiedProperties();

		currentSpeedIndex = EditorGUILayout.IntSlider(2	, 0, 5);

	}

}
