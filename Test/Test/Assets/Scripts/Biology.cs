﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biology {
	string name;
	int id;
	Anatomy anatomy;
	Body body;

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public Anatomy Anatomy {
		get {
			return this.anatomy;
		}
		set {
			anatomy = value;
		}
	}

	public Body Body {
		get {
			return this.body;
		}
		set {
			body = value;
		}
	}

}