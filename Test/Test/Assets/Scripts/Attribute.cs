﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attribute {

	private AttributeDef attributeDef;
	private int value;
	private int attributeExp;
	private int attributeLvl;

	/****   CONSTRUCTORES   ****/

	public Attribute(AttributeDef attributeDef) : this(attributeDef,0){
		
	}

	public Attribute(AttributeDef attributeDef, int value){
		this.attributeDef = attributeDef;
		this.value = value;
		this.attributeExp = 0;
		this.attributeLvl = 0;
	}

	public Attribute(Attribute attribute){
		this.attributeDef = attribute.attributeDef;
		this.value = attribute.value;
		this.attributeExp = attribute.AttributeExp;
		this.attributeLvl = attribute.AttributeLvl;
	}

	/****      METODOS      ****/

	public bool isMaxLvl(){
		return this.attributeLvl == AttributeDef.ATTRIBUTE_MAX_LVL;
	}


	public void reset(){
		this.resetValue ();
	}

	public void resetValue(){
		this.value = 0;
	}

	public void resetValueToDefault(){
		this.value = this.attributeDef.BaseValue;
	}

	public void resetExp(){
		this.attributeExp = 0;
		this.updateLvl ();
	}

	public void addExp(int exp){
		
		this.attributeExp += exp;
		this.updateLvl ();
	}

	public void substractExp(int exp){
		if (this.attributeExp != 0){
			this.attributeExp += exp;
			if (exp < 0)
				this.attributeExp = 0;
		}
		this.updateLvl ();
	
	}

	public void addValue(int value){
		this.value += value;
	}

	public void updateLvl(){
		this.attributeLvl = this.calculateAttributeLvl();
	}

	private int calculateAttributeLvl(){
		return this.attributeDef.expToLvl (this.attributeExp);
	}

	public string toShortString(){
		return this.AttributeDef.Key + " : " + this.value;
	}

	public void addAttribute(Attribute attr){
		if (attr != null)
			this.value += attr.value;
	}

	/**** SETTERS Y GETTERS ****/

	public AttributeDef AttributeDef {
		get {
			return this.attributeDef;
		}
		set {
			attributeDef = value;
		}
	}

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

	public int AttributeExp {
		get {
			return this.attributeExp;
		}
		set {
			attributeExp = value;
		}
	}

	public int AttributeLvl {
		get {
			return this.attributeLvl;
		}
		set {
			attributeLvl = value;
		}
	}
		

}


