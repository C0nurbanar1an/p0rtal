﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.Win32;

public class Service {

	private bool completed = false;
	private bool started = false;
	protected List<Character> workers;
	protected int minWorkers = 0; //A Overridear.

	/****   CONSTRUCTORES   ****/



	/****       METODOS     ****/

	public virtual int progressPerMinute (){
		return 0;
	}

	protected virtual bool isCompleted(){
		return false;
	}
	protected virtual void onCompleted (){
		
	}
	protected virtual void addProgress(int progress){
		
	}
	protected virtual void updateProgress(){
		
	}

	public void start(){
		this.started = true;
		this.notifyWorkersStartedWorking ();
	}

	private void notifyWorkersStartedWorking (){
		foreach (Character c in this.workers){
			c.notifyStartedWorking ();
		}
	}

	public void stop(){
		this.started = false;
		this.notifyWorkersStartedWorking ();
	}

	private void notifyWorkersStoppedWorking (){
		foreach (Character c in this.workers){
			c.notifyStartedWorking ();
		}
	}

	public void addWorker(Character worker){
		this.workers.Add (worker);
		if (this.workers.Count >= this.minWorkers)
			this.start();
		if (this.started){
			worker.notifyStartedWorking ();
		}
	}

	public void removeWorker(Character worker){
		this.workers.Remove (worker);
		if (this.started){
			worker.notifyStoppedWorking();
		}
	}

	protected int ProgressPerMinutes (int minutes){
		return this.progressPerMinute() * minutes;
	}


	//Llamar esta en el Update.
	protected void addProgressToService(int progress){
		this.addProgress (progress);
		this.onProgressAdded ();
	}

	protected void onProgressAdded(){
		if (this.isCompleted()){
			this.completed = true;
			this.stop ();
			//Notify.
			this.onCompleted ();
		}
	}

	public void update(){
		if (this.started){
			this.updateProgress ();
		}
	}

	/**** SETTERS Y GETTERS ****/

	public bool Completed {
		get {
			return this.completed;
		}
	}

	public List<Character> Workers {
		get {
			return this.workers;
		}
		set {
			workers = value;
		}
	}

	public bool Started {
		get {
			return this.started;
		}
	}

	public int MinWorkers {
		get {
			return this.minWorkers;
		}
	}

}
