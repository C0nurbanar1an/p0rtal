﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Timer : MonoBehaviour {

	public const int QUARTER = 0;
	public const int HALF = 1;
	public const int NORMAL = 2;
	public const int FIFTYPLUS = 3;
	public const int DOUBLE = 4;
	public const int TRIPLE = 5;

	public bool paused = true;

	public bool debugTimePassed = true;

	private float timeCounter  = 0.0f;
	private SimulationScript script;

	[SerializeField] private int currentSpeedIndex = NORMAL;

	private float[] speedMultipliers = { 0.25f, 0.5f, 1.0f, 1.5f, 2.0f, 3.0f };

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public bool setSpeed(int speed){
		if (speed < 0 || speed > 5) {
			Debug.LogError ("Speed " + speed + " is out of bounds");
			return false;
		}
		this.currentSpeedIndex = speed;
		return true;
	}

	public float currentSpeed(){
		return this.speedMultipliers [this.currentSpeedIndex];
	}

	public void pause (){
		this.paused = true;
	}

	public void unpause (){
		this.paused = false;
	}

	void Start () {
		this.script = GameObject.Find ("Simulator").GetComponent<SimulationScript> ();	
	}

	// Update is called once per frame
	void Update () {
		if (!this.paused) {
		
			float finalDeltaTime = Time.deltaTime * this.currentSpeed ();
			timeCounter += finalDeltaTime;
			if (timeCounter > 1) {
				int timePassed = (int)(timeCounter / 1);

				if (this.debugTimePassed) {
					Debug.LogWarning ("Time passed : " + timePassed);
				}

				script.onTimePassed (timePassed);
				timeCounter -= (timePassed);
			}

		}
	}

	/**** SETTERS Y GETTERS ****/

	public bool Paused {
		get {
			return this.paused;
		}
		set {
			paused = value;
		}
	}

	public bool DebugTimePassed {
		get {
			return this.debugTimePassed;
		}
		set {
			debugTimePassed = value;
		}
	}	

	public float TimeCounter {
		get {
			return this.timeCounter;
		}
	}

	public int CurrentSpeedIndex {
		get {
			return this.currentSpeedIndex;
		}
	}

	public float[] SpeedMultipliers {
		get {
			return this.speedMultipliers;
		}
	}

}
