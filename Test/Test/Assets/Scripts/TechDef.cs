﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechDef : Definition{

	private Sprite sprite;
	private int scienceCost;
	private List<TechDef> requiredTechs;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public Tech makeTech(){
		return new Tech (this);
	}

	/**** SETTERS Y GETTERS ****/

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			sprite = value;
		}
	}

	public int ScienceCost {
		get {
			return this.scienceCost;
		}
		set {
			scienceCost = value;
		}
	}

	public List<TechDef> RequiredTechs {
		get {
			return this.requiredTechs;
		}
		set {
			requiredTechs = value;
		}
	}

}
