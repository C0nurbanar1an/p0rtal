﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

	public SimulationScript simulator;       //Public variable to store a reference to the player game object
	public Vector3  currenTransform; 
	private GameObject currentGO;
	private Vector3 offset;         //Private variable to store the offset distance between the player and camera
	private float minimumY = 1f;
	private float maximumY = 4f;
	private float zoomZ = 4f;
	private Vector3 zoom;

	public void currentGameObjectChanged(GameObject go){
		this.currentGO = go;
	}

	void Start (){
		//Calculate and store the offset value by getting the distance between the player's position and camera's position.
		simulator = GameObject.Find("Simulator").GetComponent<SimulationScript>();
		offset = transform.position / 8;
	}
		
	void Update () {
		if (this.currentGO != null)
			transform.position = currentGO.transform.position + offset;




		// Mouse wheel moving forwards
		/*if(Input.GetAxis("Mouse ScrollWheel") > 0){
			//transform.position.x = 500;
			zoom = new Vector3(0, Mathf.Lerp(maximumY, minimumY, Time.time), Mathf.Lerp(transform.position.z, transform.position.z + zoomZ, Time.time));
			this.transform.position = zoom;
			print("Wheel Forward" + transform.position);
		}

		// Mouse wheel moving backwards
		if(Input.GetAxis("Mouse ScrollWheel") < 0){
			//transform.position.x = 500;
			zoom = new Vector3(0, Mathf.Lerp(minimumY, maximumY, Time.time), Mathf.Lerp(transform.position.z, transform.position.z - zoomZ, Time.time));
			this.transform.position = zoom;
			print("Wheel Backward" + transform.position);
		}*/
	}
}

