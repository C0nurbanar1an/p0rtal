﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using System;
using System.Runtime.CompilerServices;

public class DefinitionManager {

	private Deserializer YAMLDeserializer;

	public enum DefinitionType{
		ATTRIBUTE,
		SKILL,
		ABILITY,
		ITEMTYPE,
		BUILDING,
		RESOURCE,
		TECH,
		BUFF
	}

	private const string TAG = "DefinitionManager";

	//[SerializeField]
	private List<AttributeDef> attributeDefinitions;
	private List<SkillDef> skillDefinitions;
	private List<AbilityDef> abilityDefinitions;
	private List<ItemType> itemTypes;
	private List<DamageType> damageTypes;
	private List<Fabric> fabrics;
	//	-ItemSubTypes
	private List<BuildingDef> buildingDefinitions;
	private List<ResourceDef> resourceDefinitions;
	private List<TechDef> techDefinitions;
	private List<BuffDef> buffDefinitions;
	private List<QuestDef> questDefinitions;
	private List<EventDef> eventDefinitions;
	private List<CharacterConditionDef> conditionDefinitions;
	private List<ResourceRarity> resourceRarities;

	private Dictionary<DefinitionType,List<Definition>> defListDict;

	public Sprite defaultTargetPortrait;

	private static readonly string attributeDefinitionsYAMLPath = "assets/data/attributeDefinitions.yml";
	private static readonly string skillDefinitionsYAMLPath = "assets/data/skillDefinitions.yml";
	private static readonly string abilityDefinitionsYAMLPath = "assets/data/abilityDefinitions.yml";
	private static readonly string itemTypeDefinitionsYAMLPath = "assets/data/itemTypeDefinitions.yml";
	private static readonly string buildingDefinitionsYAMLPath = "assets/data/buildingDefinitions.yml";
	private static readonly string resourceDefinitionsYAMLPath = "assets/data/resourceDefinitions.yml";
	private static readonly string buffDefinitionsYAMLPath = "assets/data/buffDefinitions.yml";
	private static readonly string techDefinitionsYAMLPath = "assets/data/techDefinitions.yml";
	private static readonly string questDefinitionsYAMLPath = "assets/data/questDefinitions.yml";
	private static readonly string eventDefinitionsYAMLPath = "assets/data/eventDefinitions.yml";
	private static readonly string conditionDefinitionsYAMLPath = "assets/data/conditionDefinitions.yml";
	private static readonly string resourceRaritiesYAMLPath = "assets/data/resourceRarities.yml";
	private static readonly string damageTypesYAMLPath = "assets/data/damageTypes.yml";
	private static readonly string fabricsYAMLPath = "assets/data/fabrics.yml";

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/
	private void log(string message){	
		Debug.Log (TAG + ":" + message);
	}

	public static Sprite loadSprite(string spriteName){
		String[] split = spriteName.Split (new [] { "_" }, StringSplitOptions.None);
		Sprite sprite;
		if (split.Length > 1) { //Es de un SpriteSheet.
			String ssName = split[0];
			int resultInt;
			if (!Int32.TryParse (split [1], out resultInt)) {
				Debug.LogError ("Sprite name '" + ssName + "' has an invalid indexed format.");
				return null;
			}

			int index = Int32.Parse (split [1]);
			Sprite[] sprites = Resources.LoadAll<Sprite> ("Sprites/" + ssName);

			if (sprites.Length - 1 < index || index < 0) {
				Debug.LogError ("Sprite '" + ssName + "' index [" + index + "] is Out of Bounds.");
				return null;
			}

			return sprites [index];

		} else {
			sprite = UnityEngine.Resources.Load<Sprite>("Sprites/" + spriteName);
		}
		return sprite;
	}

	public void initialize(){
		this.initializeLists ();
		//this.initializeListDict ();
		this.buildDeserializer ();
		this.loadDefinitions ();
		this.initializeGlobalVariables ();
	}

	private void initializeGlobalVariables(){

		this.defaultTargetPortrait = loadSprite ("warcraftSprites2_23");

	}

	private void initializeLists(){
		this.attributeDefinitions = new List<AttributeDef> ();
		this.skillDefinitions = new List<SkillDef> ();
		this.abilityDefinitions = new List<AbilityDef> ();
		this.itemTypes = new List<ItemType>();
		this.buildingDefinitions = new  List<BuildingDef>();
		this.resourceDefinitions = new List<ResourceDef>();
		this.techDefinitions = new List<TechDef>();
		this.buffDefinitions = new List<BuffDef>();
		this.questDefinitions = new List<QuestDef>();
		this.eventDefinitions = new List<EventDef>();
		this.conditionDefinitions = new List<CharacterConditionDef> ();
		this.resourceRarities = new List<ResourceRarity> ();
		this.damageTypes = new List<DamageType>();
		this.fabrics = new List<Fabric> ();
	}

	private void buildDeserializer(){
		DeserializerBuilder builder = new DeserializerBuilder ();

		this.YAMLDeserializer = 
			builder.WithNamingConvention (new CamelCaseNamingConvention ())
				.Build ();
	}

	private void loadDefinitions(){
		//Cargamos los primarios y esenciales:
		this.loadAttributeDefinitions ();
		this.loadSkillDefinitions ();	
		this.loadConditionDefinitions ();
		this.loadResourceRarities ();
		this.loadResourceDefinitions ();
		this.loadFabrics ();
		this.loadDamageTypes ();
		//Cargamos los que tienen dependencias en orden.
		this.loadBuffDefinitions ();
		this.loadAbilityDefinitions ();

	}

	private void loadAttributeDefinitions(){
		string attributeDefinitionsYAML = System.IO.File.ReadAllText(attributeDefinitionsYAMLPath);
		int currentId = 0;
		AttributeDef attrDef;

		Dictionary <string,AttributeDef> attrDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, AttributeDef>>(attributeDefinitionsYAML);
		
		foreach(KeyValuePair<string, AttributeDef> keyValue in attrDict)
		{
			attrDef = keyValue.Value;
			attrDef.Key = keyValue.Key;
			attrDef.Id = currentId;
			this.attributeDefinitions.Add (attrDef);
			this.log ("Attribute Loaded - " + keyValue.Value.ToString ());
			currentId++;
		}

	}

	private void loadSkillDefinitions(){
		string skillDefinitionsYAML = System.IO.File.ReadAllText(skillDefinitionsYAMLPath);
		int currentId = 0;
		SkillDef skd;
		SkillDefTemplate skdt;

		Dictionary <string,SkillDefTemplate> skillDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, SkillDefTemplate>>(skillDefinitionsYAML);
		

		foreach(KeyValuePair<string, SkillDefTemplate> keyValue in skillDict)
		{
			skdt = keyValue.Value;
			skdt.Id = currentId;
			skdt.Key = keyValue.Key;
			skd = skdt.toSkillDef (this);
			this.skillDefinitions.Add (skd);
			this.log ("Skill Loaded - " +  skd.ToString ());
			currentId++;

		}
	}

	private void loadConditionDefinitions(){
		string conditionDefinitionsYAML = System.IO.File.ReadAllText(conditionDefinitionsYAMLPath);
		int currentId = 0;
		CharacterConditionDef ccd;
		CharacterConditionDefTemplate ccdt;

		Dictionary <string,CharacterConditionDefTemplate> ccDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, CharacterConditionDefTemplate>>(conditionDefinitionsYAML);

		foreach(KeyValuePair<string, CharacterConditionDefTemplate> keyValue in ccDict)
		{
			ccdt = keyValue.Value;
			ccdt.Key = keyValue.Key;
			ccdt.Id = currentId;
			ccd = ccdt.toDef (this);
			this.conditionDefinitions.Add (ccd);
			this.log ("Condition Loaded - " + ccd.ToString ());
			currentId++;
		}

	}

	private void loadFabrics(){
		string fabricsYAML= System.IO.File.ReadAllText(fabricsYAMLPath);
		int currentId = 0;
		Fabric fabric;

		Dictionary <string,Fabric> attrDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, Fabric>>(fabricsYAML);

		foreach(KeyValuePair<string, Fabric> keyValue in attrDict)
		{
			fabric = keyValue.Value;
			fabric.Key = keyValue.Key;
			fabric.Id = currentId;
			this.fabrics.Add (fabric);
			this.log ("Fabric Loaded - " + keyValue.Value.ToString ());
			currentId++;
		}
	}

	private void loadDamageTypes(){
		string damageTypesYAML = System.IO.File.ReadAllText(damageTypesYAMLPath);
		int currentId = 0;
		DamageTypeTemplate damageTypeTemplate;

		Dictionary <string,DamageTypeTemplate> attrDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, DamageTypeTemplate>>(damageTypesYAML);

		foreach(KeyValuePair<string, DamageTypeTemplate> keyValue in attrDict)
		{
			damageTypeTemplate = keyValue.Value;
			damageTypeTemplate.Key = keyValue.Key;
			damageTypeTemplate.Id = currentId;
			this.damageTypes.Add (damageTypeTemplate.toDamageType(this));
			this.log ("Damage Type Loaded - " + keyValue.Value.ToString ());
			currentId++;
		}
	}

	private void loadBuffDefinitions (){
		string buffDefinitionsYAML = System.IO.File.ReadAllText(buffDefinitionsYAMLPath);
		int currentId = 0;
		BuffDef bd;
		BuffDefTemplate bdt;

		Dictionary <string,BuffDefTemplate> adDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, BuffDefTemplate>>(buffDefinitionsYAML);

		foreach(KeyValuePair<string, BuffDefTemplate> keyValue in adDict)
		{
			bdt = keyValue.Value;
			bdt.Key = keyValue.Key;
			bdt.Id = currentId;
			bd = bdt.toDef (this);
			this.buffDefinitions.Add (bd);
			this.log ("Buff Loaded - " + bd.ToString ());
			currentId++;
		}
	}

	private void loadAbilityDefinitions(){
		string abilityDefinitionsYAML = System.IO.File.ReadAllText(abilityDefinitionsYAMLPath);
		int currentId = 0;
		AbilityDef ad;
		AbilityDefTemplate adt;

		Dictionary <string,AbilityDefTemplate> adDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, AbilityDefTemplate>>(abilityDefinitionsYAML);

		foreach(KeyValuePair<string, AbilityDefTemplate> keyValue in adDict)
		{
			adt = keyValue.Value;
			adt.Key = keyValue.Key;
			adt.Id = currentId;
			ad = adt.toDef (this);
			this.abilityDefinitions.Add (ad);
			this.log ("Ability Loaded - " + ad.ToString ());
			currentId++;
		}

	}

	private void loadResourceRarities(){
		string resourceRaritiesYAML = System.IO.File.ReadAllText(resourceRaritiesYAMLPath);
		int currentId = 0;
		ResourceRarity rarity;

		Dictionary <string,ResourceRarity> attrDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, ResourceRarity>>(resourceRaritiesYAML);

		foreach(KeyValuePair<string, ResourceRarity> keyValue in attrDict)
		{
			rarity = keyValue.Value;
			rarity.Key = keyValue.Key;
			rarity.Id = currentId;
			this.resourceRarities.Add (rarity);
			this.log ("ResourceRarity Loaded - " + keyValue.Value.ToString ());
			currentId++;
		}
	}

	private void loadResourceDefinitions(){
		string resourceDefinitionsYAML = System.IO.File.ReadAllText(resourceDefinitionsYAMLPath);
		int currentId = 0;
		ResourceDef rd;
		ResourceDefTemplate rdt;

		Dictionary <string,ResourceDefTemplate> skillDict = 
			this.YAMLDeserializer.Deserialize<Dictionary<string, ResourceDefTemplate>>(resourceDefinitionsYAML);


		foreach(KeyValuePair<string, ResourceDefTemplate> keyValue in skillDict)
		{
			rdt = keyValue.Value;
			rdt.Id = currentId;
			rdt.Key = keyValue.Key;
			rd = rdt.toDef (this);
			this.resourceDefinitions.Add (rd);
			this.log ("Resource Loaded - " +  rd.ToString ());
			currentId++;

		}
	}

	/**** Getters de Definiciones ****/
	/**** A traves de estos metodos las otras clases accederan 
	a las distintas definiciones contenidas en este objeto. 
	Si se usan los getters canonicos (getDefinition y getDefinitionByKey) 
	deben ser casteados del lado receptor a la definicion correspondiente.
	En caso de usar los particulares para cada Definicion particular, 
	ya devuelve el tipo correspondiente.                            ****/
			/*
	public Definition getDefinition(int id, DefinitionType defType){
		List<Definition> defList = this.defListDict [defType];
		foreach (Definition d in defList){
			if (d.Id == id){
				return d;
			}
		}
		return null;
	}
			*/

	/**** GET BY ID  ****/

	public Definition getDefinition(int id, DefinitionType defType){
		List<Definition> defList = this.defListDict [defType];
		foreach (Definition d in defList){
			if (d.Id == id){
				return d;
			}
		}
		return null;
	}

	public AttributeDef getAttributeDef(int id){
		foreach (AttributeDef ad in this.attributeDefinitions){
			if (ad.Id == id){
				return ad;
			}
		}
		return null;
	}

	public SkillDef getSkillDef(int id){
		foreach (SkillDef skd in this.skillDefinitions){
			if (skd.Id == id){
				return skd;
			}
		}
		return null;
	}

	public AbilityDef getAbilityDef(int id){
		foreach (AbilityDef ad in this.abilityDefinitions){
			if (ad.Id == id){
				return ad;
			}
		}
		return null;
	}

	public ItemType getItemType(int id){
		foreach (ItemType it in this.itemTypes){
			if (it.Id == id){
				return it;
			}
		}
		return null;
	}

	public BuildingDef getBuildingDef(int id){
		foreach (BuildingDef bd in this.buildingDefinitions){
			if (bd.Id == id){
				return bd;
			}
		}
		return null;
	}

	public ResourceRarity getResourceRarity(int id){
		foreach (ResourceRarity rr in this.resourceRarities){
			if (rr.Id == id){
				return rr;
			}
		}
		return null;
	}

	public ResourceDef getResourceDef(int id){
		foreach (ResourceDef rd in this.resourceDefinitions){
			if (rd.Id == id){
				return rd;
			}
		}
		return null;
	}

	public TechDef getTechDef(int id){
		foreach (TechDef td in this.techDefinitions){
			if (td.Id == id){
				return td;
			}
		}
		return null;
	}

	public BuffDef getBuffDef(int id){
		foreach (BuffDef bd in this.buffDefinitions){
			if (bd.Id == id){
				return bd;
			}
		}
		return null;
	}

	public EventDef getEventDef(int id){
		foreach (EventDef ev in this.eventDefinitions){
			if (ev.Id == id){
				return ev;
			}
		}
		return null;
	}

	public QuestDef getQuestDef(int id){
		foreach (QuestDef qd in this.questDefinitions){
			if (qd.Id == id){
				return qd;
			}
		}
		return null;
	}

	public CharacterConditionDef getCharacterConditionDef(int id){
		foreach (CharacterConditionDef cd in this.conditionDefinitions){
			if (cd.Id == id){
				return cd;
			}
		}
		return null;
	}

	public DamageType getDamageType(int id){
		foreach (DamageType dt in this.damageTypes){
			if (dt.Id == id){
				return dt;
			}
		}
		return null;
	}

	public Fabric getFabric(int id){
		foreach (Fabric fab in this.fabrics){
			if (fab.Id == id){
				return fab;
			}
		}
		return null;
	}

	/**** GET BY KEY  ****/

	public Definition getDefinition(string key, DefinitionType defType){
		List<Definition> defList = this.defListDict [defType];
		foreach (Definition d in defList){
			if (d.Key == key){
				return d;
			}
		}
		return null;
	}

	public AttributeDef getAttributeDef(string key){
		foreach (AttributeDef ad in this.attributeDefinitions){
			if (ad.Key == key){
				return ad;
			}
		}
		return null;
	}

	public SkillDef getSkillDef(string key){
		foreach (SkillDef skd in this.skillDefinitions){
			if (skd.Key == key){
				return skd;
			}
		}
		return null;
	}

	public AbilityDef getAbilityDef(string key){
		foreach (AbilityDef ad in this.abilityDefinitions){
			if (ad.Key == key){
				return ad;
			}
		}
		return null;
	}

	public ItemType getItemType(string key){
		foreach (ItemType it in this.itemTypes){
			if (it.Key == key){
				return it;
			}
		}
		return null;
	}

	public BuildingDef getBuildingDef(string key){
		foreach (BuildingDef bd in this.buildingDefinitions){
			if (bd.Key == key){
				return bd;
			}
		}
		return null;
	}

	public ResourceRarity getResourceRarity(string key){
		foreach (ResourceRarity rr in this.resourceRarities){
			if (rr.Key == key){
				return rr;
			}
		}
		return null;
	}

	public ResourceDef getResourceDef(string key){
		foreach (ResourceDef rd in this.resourceDefinitions){
			if (rd.Key == key){
				return rd;
			}
		}
		return null;
	}

	public TechDef getTechDef(string key){
		foreach (TechDef td in this.techDefinitions){
			if (td.Key == key){
				return td;
			}
		}
		return null;
	}

	public BuffDef getBuffDef(string key){
		foreach (BuffDef bd in this.buffDefinitions){
			if (bd.Key == key){
				return bd;
			}
		}
		return null;
	}

	public EventDef getEventDef(string key){
		foreach (EventDef ev in this.eventDefinitions){
			if (ev.Key == key){
				return ev;
			}
		}
		return null;
	}

	public QuestDef getQuestDef(string key){
		foreach (QuestDef qd in this.questDefinitions){
			if (qd.Key == key){
				return qd;
			}
		}
		return null;
	}

	public CharacterConditionDef getCharacterConditionDef(string key){
		foreach (CharacterConditionDef cd in this.conditionDefinitions){
			if (cd.Key == key){
				return cd;
			}
		}
		return null;
	}

	public DamageType getDamageType(string key){
		foreach (DamageType dt in this.damageTypes){
			if (dt.Key == key){
				return dt;
			}
		}
		return null;
	}

	public Fabric getFabric(string key){
		foreach (Fabric fab in this.fabrics){
			if (fab.Key == key){
				return fab;
			}
		}
		return null;
	}

	/****  CLASES INTERNAS  ****/
	/****  Estas clases se usan como templates para el YAML, 
	los cuales esta clase transformara en las clases correspondientes. 
	Estas clases son tomadas como modelo por el Deserializer para volcar
	los datos del YAML.                                            ****/

	private class SkillDefTemplate : Definition{
		//private List<int> lvlExpMilestones;
		private AttributeFormulaTemplate attributeFormula;
		private AbilityMap abilityMap;

		public SkillDef toSkillDef(DefinitionManager definitions){
			SkillDef skd = new SkillDef ();
			skd.Id = this.id;
			skd.Name = this.name;
			skd.Description = this.description;
			skd.Key = this.key;
			skd.AttributeFormula = this.AttributeFormula.toAttributeFormula (definitions);
			return skd;
		}
			
		public AttributeFormulaTemplate AttributeFormula {
			get {
				return this.attributeFormula;
			}
			set {
				attributeFormula = value;
			}
		}

		public AbilityMap AbilityMap {
			get {
				return this.abilityMap;
			}
			set {
				abilityMap = value;
			}
		}

	}

	private class CharacterConditionDefTemplate : Definition{

		private int baseMaxValue;
		private int baseMinValue;
		private AttributeFormulaTemplate attributeFormula;

		public CharacterConditionDef toDef(DefinitionManager definitions){
			CharacterConditionDef ccd = new CharacterConditionDef ();
			ccd.Id = this.id;
			ccd.Name = this.name;
			ccd.Description = this.description;
			ccd.Key = this.key;
			ccd.AttributeFormula = this.attributeFormula.toAttributeFormula (definitions);
			return ccd;
		}

		public int BaseMaxValue {
			get {
				return this.baseMaxValue;
			}
			set {
				baseMaxValue = value;
			}
		}

		public int BaseMinValue {
			get {
				return this.baseMinValue;
			}
			set {
				baseMinValue = value;
			}
		}

		public AttributeFormulaTemplate AttributeFormula {
			get {
				return this.attributeFormula;
			}
			set {
				attributeFormula = value;
			}
		}

	}


	private class BuffDefTemplate : Definition {
		string buffType;
		int baseDuration;
		float cureCostMultiplier;
		List<IterativeEffectTemplate> iterativeEffects;
		List<CharacterEffectTemplate> constantEffects;
		string spriteName;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		public BuffDef toDef(DefinitionManager definitions){

			BuffDef bd = new BuffDef();
			bd.Id = this.id;
			bd.Name = this.name;
			bd.Description = this.description;
			bd.Key = this.key;
			bd.Type = this.keyToBuffType (this.buffType);
			bd.BaseDuration = this.baseDuration;


			Sprite sprite = loadSprite(this.spriteName);
			if (sprite == null){
				Debug.LogError ("Buff Sprite '" + this.spriteName + "' Not found on Sprites Folder (Resources/Sprites/)");
			}

			bd.Sprite = sprite;

			//Assign iterative Effects.
			if (this.iterativeEffects != null){
				foreach (IterativeEffectTemplate iet in this.iterativeEffects){
					bd.addIterativeEffect(iet.toIterativeEffect(definitions));
				}
			}

			//Assign constant effects.
			if (this.constantEffects != null){
				foreach (CharacterEffectTemplate cet in this.constantEffects){
					bd.addConstantEffect(cet.toEffect(definitions));
				}
			}

			if (!this.constantEffectsAreOK (bd.ConstantEffects)) {
				Debug.LogError ("Buff " + this.name + " has Invalid Constant Effects (Non Constant when they should be)");
				return null;
			}
			if (!this.iterativeEffectsAreOK (bd.IterativeEffects)) {
				Debug.LogError ("Buff " + this.name + " has Invalid Iterative Effects (Non Iterative when they should be)");
				return null;
			}

			return bd;

		}

		private bool constantEffectsAreOK (List<CharacterEffect> effects){
			foreach (CharacterEffect effect in effects) {
				if (!effect.isConstantEffect ()) {
					return false;
				}
			}
			return true;
		}

		private bool iterativeEffectsAreOK(List<BuffDef.IterativeEffectDef> effects){
			foreach (BuffDef.IterativeEffectDef effect in effects) {
				if (effect.Effect.isConstantEffect()) {
					return false;
				}
			}
			return true;
		}

		private BuffDef.BuffType keyToBuffType (string key){
			//TODO Implementar esto.
			return BuffDef.BuffType.NULL;

		}



		/**** SETTERS Y GETTERS ****/


		public string BuffType {
			get {
				return this.buffType;
			}
			set {
				buffType = value;
			}
		}

		public int BaseDuration {
			get {
				return this.baseDuration;
			}
			set {
				baseDuration = value;
			}
		}

		public float CureCostMultiplier {
			get {
				return this.cureCostMultiplier;
			}
			set {
				cureCostMultiplier = value;
			}
		}

		public List<IterativeEffectTemplate> IterativeEffects {
			get {
				return this.iterativeEffects;
			}
			set {
				iterativeEffects = value;
			}
		}

		public List<CharacterEffectTemplate> ConstantEffects {
			get {
				return this.constantEffects;
			}
			set {
				constantEffects = value;
			}
		}

		public string SpriteName {
			get {
				return this.spriteName;
			}
			set {
				spriteName = value;
			}
		}

	}

	private class IterativeEffectTemplate {
		private CharacterEffectTemplate effect;
		private int frequency;


		public BuffDef.IterativeEffectDef toIterativeEffect(DefinitionManager definitions){

			BuffDef.IterativeEffectDef def = new BuffDef.IterativeEffectDef ();
			def.Effect = this.effect.toEffect (definitions);
			def.Frequency = this.frequency;
			return def;

		}

		public CharacterEffectTemplate Effect {
			get {
				return this.effect;
			}
			set {
				effect = value;
			}
		}

		public int Frequency {
			get {
				return this.frequency;
			}
			set {
				frequency = value;
			}
		}

	}


	private class AbilityDefTemplate : Definition{

		string skillDefKey;
		int baseCooldown;
		Dictionary <string,int> characterConditionCost;
		int apCost;
		int range;
		string targetType;
		string spriteName;

		List<string> requiredAbilites; 
		List<CharacterEffectTemplate> casterEffects;
		List<CharacterEffectTemplate> targetEffects;

		public AbilityDef toDef(DefinitionManager definitions){
			AbilityDef ad = new AbilityDef();
			SkillDef skd;
			ad.Id = this.id;
			ad.Name = this.name;
			ad.Description = this.description;
			ad.Key = this.key;
			ad.TargetType = this.keyToTargetType (this.targetType);
			ad.Range = this.range;
			ad.CharacterConditionCost = this.makeConditionCostsFromKeys (definitions);
			skd = definitions.getSkillDef (this.skillDefKey);

			Sprite sprite = loadSprite(this.spriteName);

			if (sprite == null) {
				Debug.LogError ("Sprite '" + this.spriteName + "' Not found on Sprites Folder (Resources/Sprites/)");
				//Application.Quit ();
			}
			ad.Sprite = sprite;

			//Get and Assign SkillDef from Definitions.
			if (skd == null) {
				//ERROR!
				Debug.LogError ("SkillDefKey '" + this.skillDefKey + "' Not found on Skill Definitions!");	
				Application.Quit ();
			} else { 
				ad.SkillDef = skd;
			}

			//Assign effects on Caster.
			if (this.casterEffects != null){
				foreach (CharacterEffectTemplate cet in this.casterEffects){
					ad.addCasterEffect(cet.toEffect(definitions));
				}
			}

			//Assign effects on Target.
			if (this.targetEffects != null){
				foreach (CharacterEffectTemplate cet in this.targetEffects){
					ad.addTargetEffect(cet.toEffect(definitions));
				}
			}

			return ad;

		}

		private AbilityDef.AbilityTargetType keyToTargetType (string key){
		
			switch (key) {

			case "SELF_CAST":
				return AbilityDef.AbilityTargetType.SELF_CAST;
			case "CHAR_TARGETED":
				return AbilityDef.AbilityTargetType.CHAR_TARGETED;
			case "GROUND_TARGETED":
				return AbilityDef.AbilityTargetType.GROUND_TARGETED;
			default:
				//Error! 
				Debug.LogError ("CastTypeKey '" + key + "' Not found on available cast types.");	
				Application.Quit ();
				return AbilityDef.AbilityTargetType.NULL;
			}

		}

		private Dictionary <CharacterConditionDef,int> makeConditionCostsFromKeys(DefinitionManager definitions){
			Dictionary <CharacterConditionDef,int> result = new Dictionary <CharacterConditionDef,int> ();
			CharacterConditionDef ccd;
			foreach (KeyValuePair<string,int> keyValue in this.characterConditionCost) {
				ccd = definitions.getCharacterConditionDef (keyValue.Key);
				if (ccd == null) {
					Debug.LogError ("CharacterConditionKey '" + keyValue.Key + "' Not found on Condition Definitions");
					Application.Quit ();
				} else {
					result.Add (ccd,keyValue.Value);
				}
			}

			return result;
		}

		public string SkillDef {
			get {
				return this.skillDefKey;
			}
			set {
				this.skillDefKey = value;
			}
		}

		public int BaseCooldown {
			get {
				return this.baseCooldown;
			}
			set {
				baseCooldown = value;
			}
		}

		public Dictionary<string, int> CharacterConditionCost {
			get {
				return this.characterConditionCost;
			}
			set {
				characterConditionCost = value;
			}
		}

		public int ApCost {
			get {
				return this.apCost;
			}
			set {
				apCost = value;
			}
		}

		public string TargetType {
			get {
				return this.targetType;
			}
			set {
				targetType = value;
			}
		}

		public List<string> RequiredAbilites {
			get {
				return this.requiredAbilites;
			}
			set {
				requiredAbilites = value;
			}
		}

		public List<CharacterEffectTemplate> CasterEffects {
			get {
				return this.casterEffects;
			}
			set {
				casterEffects = value;
			}
		}

		public List<CharacterEffectTemplate> TargetEffects {
			get {
				return this.targetEffects;
			}
			set {
				targetEffects = value;
			}
		}

		public string Sprite {
			get {
				return this.spriteName;
			}
			set {
				spriteName = value;
			}
		}

		public int Range {
			get {
				return this.range;
			}
			set {
				range = value;
			}
		}

	}

	private class CharacterEffectTemplate {

		//This class is used as a template for all possible effects 
		//put into the YAML file.

		//Ver si sirve pasarlo a CharacterEffect.
		private enum EffectTypeEnum {
			DEAL_DAMAGE,
			HEAL_CHARACTER,
			ALTER_CONDITION,
			ALTER_ATTRIBUTE,
			ALTER_SKILL,
			ADD_BUFF
		}

		private CharacterEffectFormulaTemplate formula; //Formula.
		private string effectType; //REQUIRED. Used to define which effect is this.
		private string damageType; //Property of DealDamageEffect.
		private string condition; //Property of AlterConditionEffect.
		private string attribute; //Property of AlterAttributeEffect.
		private string skill; //Property of AlterSkillEffect.
		private string buff; //Property of AddBuffEffect.

		//private string boundary; //Property of AlterConditionEffect.

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		public CharacterEffect toEffect (DefinitionManager definitions){
			CharacterEffect effect;
			switch (this.effectType) {
			case "DEAL_DAMAGE":
				
				DealDamageEffect ddEffect = new DealDamageEffect ();
				ddEffect.DamageType = this.damageTypeFromString (definitions);
				ddEffect.Formula = this.formula.toFormula(definitions);

				effect  = ddEffect;
				break;

			case "HEAL_CHARACTER":

				HealCharacterEffect hcEffect = new HealCharacterEffect ();
				hcEffect.Formula = this.formula.toFormula(definitions);

				effect = hcEffect;
				break;

			case "ALTER_CONDITION":
				AlterCharacterConditionEffect accEffect = new AlterCharacterConditionEffect ();
				accEffect.CharacterConditionDef = this.conditionFromString (definitions);
				accEffect.Formula = this.formula.toFormula (definitions);

				effect = accEffect;
				break;
			
			case "ALTER_ATTRIBUTE":
				AlterCharacterAttributeEffect actEffect = new AlterCharacterAttributeEffect ();
				actEffect.AttributeDef = this.attributeFromString(definitions);
				actEffect.Formula = this.formula.toFormula (definitions);

				effect = actEffect;
				break;
			
			case "ALTER_SKILL":
				AlterCharacterSkillEffect acsEffect = new AlterCharacterSkillEffect ();
				acsEffect.SkillDef = this.skillFromString(definitions);
				acsEffect.Formula = this.formula.toFormula (definitions);

				effect = acsEffect;
				break;
			
			case "ADD_BUFF":
				AddBuffEffect abEffect = new AddBuffEffect();
				abEffect.BuffDef = this.buffFromString(definitions);
				//abEffect.Formula = this.formula.toFormula (definitions);

				effect = abEffect;
				break;

			default:
				Debug.LogError ("Ability effect Type not valid!" + Environment.NewLine
				+ "Type : " + this.effectType + Environment.NewLine);
				effect = null;
				break;
			}

			return effect;

		}

		private BuffDef buffFromString(DefinitionManager definitions){
			BuffDef def;
			def = definitions.getBuffDef (this.buff);
			if (def == null) {
				Debug.LogError ("Buff not found on Definitions! " + Environment.NewLine
					+ "Key : " + this.buff + Environment.NewLine);
			}
			return def;
		}

		private AttributeDef attributeFromString(DefinitionManager definitions){
			AttributeDef attribute;
			attribute = definitions.getAttributeDef (this.attribute);
			if (attribute == null) {
				Debug.LogError ("Attribute not found on Definitions! " + Environment.NewLine
					+ "Key : " + this.attribute+ Environment.NewLine);
			}
			return attribute;
		}

		private SkillDef skillFromString(DefinitionManager definitions){
			SkillDef skill;
			skill = definitions.getSkillDef (this.skill);
			if (skill == null) {
				Debug.LogError ("Skill not found on Definitions! " + Environment.NewLine
					+ "Key : " + this.skill + Environment.NewLine);
			}
			return skill;
		}

		private CharacterConditionDef conditionFromString(DefinitionManager definitions){
			CharacterConditionDef condition;
			condition = definitions.getCharacterConditionDef (this.condition);
			if (condition == null) {
				Debug.LogError ("Condition not found on Definitions! " + Environment.NewLine
				+ "Key : " + this.condition + Environment.NewLine);
			}
			return condition;

		}

		private DamageType damageTypeFromString(DefinitionManager definitions){
			DamageType damageType;
			damageType = definitions.getDamageType (this.damageType);
			if (damageType == null) {
				Debug.LogError ("Damage Type not found on Definitions! " + Environment.NewLine
					+ "Key : " + this.damageType + Environment.NewLine);
			}
			return damageType;
		}



		/**** SETTERS Y GETTERS ****/

		public CharacterEffectFormulaTemplate Formula {
			get {
				return this.formula;
			}
			set {
				formula = value;
			}
		}

		public string EffectType {
			get {
				return this.effectType;
			}
			set {
				effectType = value;
			}
		}

		public string DamageType {
			get {
				return this.damageType;
			}
			set {
				damageType = value;
			}
		}

		public string Condition {
			get {
				return this.condition;
			}
			set {
				condition = value;
			}
		}

		public string Attribute {
			get {
				return this.attribute;
			}
			set {
				attribute = value;
			}
		}

		public string Skill {
			get {
				return this.skill;
			}
			set {
				skill = value;
			}
		}

		public string Buff {
			get {
				return this.buff;
			}
			set {
				buff = value;
			}
		}

	}

	private class CharacterEffectFormulaTemplate {

		List<CharacterInfluenceTemplate> casterInfluences;
		List<CharacterInfluenceTemplate> targetInfluences;
		int fixedValue;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		public CharacterEffectFormula toFormula(DefinitionManager definitions){

			CharacterEffectFormula formula = new CharacterEffectFormula ();

			formula.FixedValue = this.fixedValue;
			if (this.casterInfluences != null) {
				foreach (CharacterInfluenceTemplate cit in this.casterInfluences) {
					this.addTemplateInfluenceToCharacterInfluences (cit, formula.CasterInfluences, definitions);
				}
			}
			if (this.targetInfluences != null) {
				foreach (CharacterInfluenceTemplate cit in this.targetInfluences) {
					this.addTemplateInfluenceToCharacterInfluences (cit, formula.TargetInfluences, definitions);
				}
			}
			return formula;

		}

		private void addTemplateInfluenceToCharacterInfluences(
			CharacterInfluenceTemplate cit, 
			CharacterInfluences influences,
			DefinitionManager definitions){

			switch (cit.Type) {

			case "ATTRIBUTE":

				AttributeInfluence ai = new AttributeInfluence();
				AttributeDef ad = definitions.getAttributeDef(cit.Key);
				if (ad == null){
					Debug.LogError("AttributeDef Key '" + ad.Key + "' Not found on Attribute Definitions!");
				}

				ai.AttributeDef = ad;
				ai.VirtueStatus = this.statusFromString(cit.Status);
				ai.Multiplier = cit.Multiplier;

				influences.addAttributeInfluence(ai);

				break;
			case "SKILL":

				SkillInfluence ski  = new SkillInfluence();
				SkillDef skd = definitions.getSkillDef(cit.Key);
				if (skd == null){
					Debug.LogError("SkillDef Key '" + skd.Key + "' Not found on Skill Definitions!");
				}

				ski.SkillDef= skd;
				ski.VirtueStatus = this.statusFromString(cit.Status);
				ski.Multiplier = cit.Multiplier;

				influences.addSkillInfluence(ski);

				break;
			case "CONDITION":

				ConditionInfluence ci = new ConditionInfluence();
				CharacterConditionDef ccd = definitions.getCharacterConditionDef(cit.Key);
				if (ccd == null){
					Debug.LogError("ConditionDef Key '" + ccd.Key + "' Not found on Condition Definitions!");
				}

				ci.ConditionDef = ccd;
				ci.Boundary = this.boundaryFromString(cit.Boundary);
				ci.Multiplier = cit.Multiplier;

				influences.addConditionInfluence(ci);

				break;
			default:
				Debug.LogError ("CharacterInfluenceTemplate type '" + cit.Type + "' is not valid!");
				break;

			}

		}

		private CharacterVirtueInfluence.VirtueStatusEnum statusFromString(string status){

			CharacterVirtueInfluence.VirtueStatusEnum result;

			switch (status){

			case "BASE":
				result = CharacterVirtueInfluence.VirtueStatusEnum.BASE;
				break;

			case "EXTRA":
				result = CharacterVirtueInfluence.VirtueStatusEnum.EXTRA;
				break;

			case "FINAL":
				result = CharacterVirtueInfluence.VirtueStatusEnum.FINAL;
				break;
			
			default:
				Debug.LogError ("Character Virtue Status '" + status + "' is not valid!");
				result = CharacterVirtueInfluence.VirtueStatusEnum.NULL;
				break;

			}

			return result;

		}

		private ConditionInfluence.BoundaryType boundaryFromString(string boundary){

			ConditionInfluence.BoundaryType result;

			switch (boundary){

			case "MAX":
				result = ConditionInfluence.BoundaryType.MAX;
				break;

			case "CURRENT":
				result = ConditionInfluence.BoundaryType.CURRENT;
				break;

			default:
				Debug.LogError ("Condition Boundary '" + boundary + "' is not valid!");
				result = ConditionInfluence.BoundaryType.NULL;
				break;

			}
			return result;
		}

		/**** SETTERS Y GETTERS ****/

		public List<CharacterInfluenceTemplate> CasterInfluences {
			get {
				return this.casterInfluences;
			}
			set {
				casterInfluences = value;
			}
		}

		public List<CharacterInfluenceTemplate> TargetInfluences {
			get {
				return this.targetInfluences;
			}
			set {
				targetInfluences = value;
			}
		}

		public int FixedValue {
			get {
				return this.fixedValue;
			}
			set {
				fixedValue = value;
			}
		}

	}

	private class CharacterInfluenceTemplate {

		string type;
		string key;
		string status;
		string boundary;
		float multiplier;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/



		/**** SETTERS Y GETTERS ****/

		public string Type {
			get {
				return this.type;
			}
			set {
				type = value;
			}
		}

		public string Key {
			get {
				return this.key;
			}
			set {
				key = value;
			}
		}

		public float Multiplier {
			get {
				return this.multiplier;
			}
			set {
				multiplier = value;
			}
		}

		public string Status {
			get {
				return this.status;
			}
			set {
				status = value;
			}
		}

		public string Boundary {
			get {
				return this.boundary;
			}
			set {
				boundary = value;
			}
		}


	}

	private class AttributeFormulaTemplate{
		int baseValue;
		Dictionary<string,float> attributeInfluences;

		public AttributeFormula toAttributeFormula(DefinitionManager definitions){
			AttributeFormula skf = new AttributeFormula();
			skf.BaseValue = this.baseValue;
			AttributeInfluence attrInf;
			foreach(KeyValuePair<string, float> keyValue in attributeInfluences){
				attrInf = new AttributeInfluence();
				attrInf.AttributeDef = definitions.getAttributeDef(keyValue.Key);
				if (attrInf.AttributeDef == null){ //ERROR!
					Debug.LogError("AttributeKey '" + keyValue.Key + "' Not found on Attribute Definitions");
					Application.Quit ();
				}
				attrInf.Multiplier = keyValue.Value;
				skf.AttributeInfluences.Add(attrInf);
			}
			return skf;
		}

		public int BaseValue {
			get {
				return this.baseValue;
			}
			set {
				baseValue = value;
			}
		}

		public Dictionary<string, float> AttributeInfluences {
			get {
				return this.attributeInfluences;
			}
			set {
				attributeInfluences = value;
			}
		}

	}

	private class DamageTypeTemplate : Definition {

		private Dictionary <string, float> fabricDamageMultipliers;

		/****   CONSTRUCTORES   ****/



		/****       METODOS     ****/

		public DamageType toDamageType(DefinitionManager definitions){

			DamageType dt = new DamageType ();
			dt.Id = this.id;
			dt.Key = this.key;
			dt.Description = this.description;
			dt.Name = this.name;

			dt.FabricDamageMultipliers = this.makeFabricDamageMultipliers (definitions);

			return dt;

		}

		private Dictionary<Fabric,float> makeFabricDamageMultipliers(DefinitionManager definitions){

			return null;

		}

		/**** SETTERS Y GETTERS ****/

		public Dictionary<string, float> FabricDamageMultipliers {
			get {
				return this.fabricDamageMultipliers;
			}
			set {
				fabricDamageMultipliers = value;
			}
		}

	}

	public class ResourceDefTemplate : Definition{

		string spriteName;
		string rarityKey;

		/****   CONSTRUCTORES   ****/

		/****       METODOS     ****/

		public ResourceDef toDef(DefinitionManager definitions){

			ResourceDef rd = new ResourceDef();
			ResourceRarity rr;
			Sprite sprite;
			rr = definitions.getResourceRarity(this.rarityKey);
			if (rr == null) {
				Debug.LogError ("ResourceRarity Key '" + this.rarityKey + "' Not found on System Resource Rarities");
				Application.Quit ();
			}
			rd.Rarity = rr;

			sprite = loadSprite(this.spriteName);

			if (sprite == null) {
				Debug.LogError ("Sprite '" + this.spriteName + "' Not found on Sprites Folder (Resources/Sprites/)");
				Application.Quit ();
			}
			rd.Id = this.id;
			rd.Name = this.name;
			rd.Description = this.description;
			rd.Sprite = sprite;
			return rd;
		}

		/**** SETTERS Y GETTERS ****/

		public string Sprite {
			get {
				return this.spriteName;
			}
			set {
				spriteName = value;
			}
		}

		public string Rarity {
			get {
				return this.rarityKey;
			}
			set {
				rarityKey = value;
			}
		}

	}

	/**** SETTERS Y GETTERS ****/

	public List<AttributeDef> AttributeDefinitions {
		get {
			return this.attributeDefinitions;
		}
	}

	public List<SkillDef> SkillDefinitions {
		get {
			return this.skillDefinitions;
		}
	}

	public List<AbilityDef> AbilityDefinitions {
		get {
			return this.abilityDefinitions;
		}
	}

	public List<ItemType> ItemTypes {
		get {
			return this.itemTypes;
		}
	}

	public List<DamageType> DamageTypes {
		get {
			return this.damageTypes;
		}
	}

	public List<Fabric> Fabrics {
		get {
			return this.fabrics;
		}
	}

	public List<BuildingDef> BuildingDefinitions {
		get {
			return this.buildingDefinitions;
		}
	}

	public List<ResourceDef> ResourceDefinitions {
		get {
			return this.resourceDefinitions;
		}
	}

	public List<TechDef> TechDefinitions {
		get {
			return this.techDefinitions;
		}
	}

	public List<BuffDef> BuffDefinitions {
		get {
			return this.buffDefinitions;
		}
	}

	public List<QuestDef> QuestDefinitions {
		get {
			return this.questDefinitions;
		}
	}

	public List<EventDef> EventDefinitions {
		get {
			return this.eventDefinitions;
		}
	}

	public List<CharacterConditionDef> ConditionDefinitions {
		get {
			return this.conditionDefinitions;
		}
	}

	public List<ResourceRarity> ResourceRarities {
		get {
			return this.resourceRarities;
		}
	}

}
