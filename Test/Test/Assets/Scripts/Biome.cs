﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Biome {

	private int id;
	private string name;
	private int averageTemperature;
	private Flora flora;
	private Fauna fauna;

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public int AverageTemperature {
		get {
			return this.averageTemperature;
		}
		set {
			averageTemperature = value;
		}
	}

	public Flora Flora {
		get {
			return this.flora;
		}
		set {
			flora = value;
		}
	}

	public Fauna Fauna {
		get {
			return this.fauna;
		}
		set {
			fauna = value;
		}
	}

}