﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class CombatController : GameController {

	public enum eActionState{
		CHAR_TARGET_ABILITY,
		GROUND_TARGET_ABILITY,
		IDLE,
		NULL
	}

	private Combat combat;
	private eActionState actionState;
	private Ability readiedAbility;
	private Character target;

	/****   CONSTRUCTORES   ****/

	public CombatController (GameManager gameManager, Combat combat){
		this.combat = combat;
		this.gameManager = gameManager;
		this.ui = gameManager.Ui;
		this.actionState = eActionState.IDLE;
		combat.startCombat();
		this.updateCurrentCharacter ();
		//this.gameManager.CurrentCharacter = combat.CurrentCharacter;
		//this.ui.updatePartyPanel ();
		this.updateUI();

	}

	/****       METODOS     ****/

	public override void finalize(){
		this.deselectCurrentCharacter ();
	}

	protected override void updateUI(){
		this.setCombatModeButtonSpriteToExploration ();
		this.ui.showEndTurnButton ();
	}

	private void setCombatModeButtonSpriteToExploration(){

		this.ui.setCombatModeButtonSprite (DefinitionManager.loadSprite ("warcraftSprites2_22"));

	}

	protected override void update(){
		//this.currentCharacter = this.combat.CurrentCharacter;

	}

	public override void onCombatButtonPressed(){
		this.gameManager.enterExplorationMode ();
	}

	public override void onQuickAbilityButtonPressed(int index){
		this.onAbilityReadied(new Ability(this.gameManager.DefinitionManager.getAbilityDef(index)));
	}

	public void onAbilityReadied(Ability ability){
		switch (ability.AbilityDef.TargetType){

		case AbilityDef.AbilityTargetType.SELF_CAST:

			this.combat.CurrentCharacter.castAbility(ability,this.combat.CurrentCharacter);
			//Update UI for AP and shit.
			break;

		case AbilityDef.AbilityTargetType.CHAR_TARGETED:

			this.ui.printConsoleMsg ("Ability readied : " + ability.AbilityDef.Name);
			this.readiedAbility = ability;
			this.setActionState(eActionState.CHAR_TARGET_ABILITY);
			break;

		case AbilityDef.AbilityTargetType.GROUND_TARGETED:

			this.ui.printConsoleMsg ("Ability readied : " + ability.AbilityDef.Name);
			this.readiedAbility = ability;
			//this.setCursor(Radio/Cone/area/etc);
			this.setActionState(eActionState.GROUND_TARGET_ABILITY);
			break;

		default:
			//esto nunca deberia pasar.
			break;
		}
	}
		
	public override void onCharacterClicked(CharacterMB clickedCharacterMB,int clickType){

		switch(this.actionState){

		case eActionState.CHAR_TARGET_ABILITY:
			if (clickType == LEFT_CLICK){

				this.ui.printConsoleMsg (this.combat.CurrentCharacter.Biography.Name
					+ " Casting " + this.readiedAbility.AbilityDef.Name
					+ " To " + clickedCharacterMB.Character.Biography.Name);

				this.combat.CurrentCharacter.castAbility(this.readiedAbility, clickedCharacterMB.Character);	

				this.readiedAbility = null;
				this.setActionState (eActionState.IDLE);

			}
			break;

		case eActionState.IDLE:
			//Acceder a informacion detallada quizas.

		default:
			break;
		}

	}

	public override void characterOnMouseEnter(CharacterMB characterMB){
		this.target = characterMB.Character;
		int color;

		if (this.target.Party == this.gameManager.CurrentParty) {
			color = 1;
		} else {
			color = 0;
		}

		characterMB.enableOutline (color);
		this.ui.showTargetPanel (characterMB);
	}

	public override void characterOnMouseExit(CharacterMB characterMB){
		this.target = null;
		this.targetLost ();
	}

	private void targetLost(){
		this.ui.hideTargetPanel ();
	}

	public void setActionState(eActionState actionState){
		this.actionState = actionState;
		this.ui.printConsoleMsg ("Action State Changed to : " + Enum.GetName (typeof(eActionState), this.actionState));
	}

	public void cancelAction(){
		this.setActionState(eActionState.IDLE);
		//Update UI.
	}
		
	public void currentCharacterTurnEnded(){
		this.combat.currentCharacterEndedTurn ();
	}

	public override void onEndTurnButtonPressed(){
		this.endTurn ();
	}

	public void endTurn (){
		this.goToNextCharacter ();

	}

	private void updateCurrentCharacter(){
		this.combat.CurrentCharacter.CharacterMB.enableOutline2 (UI.BLUE);
		this.gameManager.setCurrentCharacter (this.combat.CurrentCharacter);
	}

	private void goToNextCharacter(){
		this.deselectCurrentCharacter ();
		this.combat.goToNextCharacter ();
		this.updateCurrentCharacter ();
	}

	private void deselectCurrentCharacter(){
		this.combat.CurrentCharacter.CharacterMB.disableOutline2 ();
	}

	/**** SETTERS Y GETTERS ****/



}
