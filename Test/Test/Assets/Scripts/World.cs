﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World {

	string name;
	long seed;

	public World (){
		
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public long Seed {
		get {
			return this.seed;
		}
		set {
			seed = value;
		}
	}
	


}