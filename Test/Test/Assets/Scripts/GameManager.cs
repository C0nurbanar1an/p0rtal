﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Schema;
using System;

public class GameManager : MonoBehaviour {



	public enum ePartyState {
		EXPLORATION,
		COMBAT
	}

	private ePartyState partyState;

	private Combat combatManager;
	private UI ui;
	private DefinitionManager definitionManager;
	private Party currentParty;
	private Character currentCharacter;
	private Character target;
	private GameController currentController;

	/****   CONSTRUCTORES   ****/


	/****       METODOS     ****/



	public void setCurrentParty(Party party){
		this.currentParty = party;
	}

	public void printConsoleMsg(string msg){
		this.ui.printConsoleMsg (msg);
	}

	public void setPartyState(ePartyState partyState){
		this.partyState = partyState;
		this.printConsoleMsg ("Party State Changed to : " + Enum.GetName (typeof(ePartyState), this.partyState));
		this.setCorrespondingController ();
	}

	private void setCorrespondingController(){
		switch (this.partyState){
		case ePartyState.COMBAT:
			

			break;

		case ePartyState.EXPLORATION:

			break;
		
		}
	}

	public void enterCombatMode(){
		this.setPartyState(ePartyState.COMBAT);
		Party enemyParty = GameObject.Find("Simulator").GetComponent<SimulationScript>().Party1;
		List<Party> combatParties = new List<Party> ();
		combatParties.Add (this.currentParty);
		combatParties.Add (enemyParty);

		this.currentController = new CombatController (this, new Combat(combatParties));
	}

	public void exitCombatMode(){

	}

	public void enterExplorationMode(){
		this.currentController.finalize ();
		this.setPartyState (ePartyState.EXPLORATION);
		this.currentController = new ExplorationController (this);
	}

	public void setCurrentCharacter(Character character){
		this.currentCharacter = character;
		//this.worldUI.setCurrentCharacter (character);
		this.printConsoleMsg("Current Character Selected : " + character.Biography.Name);
		this.ui.updatePartyPanel ();
	}

	private void startDefinitionManager(){
		this.definitionManager = new DefinitionManager ();
		this.initializeDefinitionManager ();
	}

	private void initializeDefinitionManager(){
		this.definitionManager.initialize ();
	}

	void Awake(){
		this.startDefinitionManager ();
	}

	void Start () {
		this.ui = GameObject.Find ("UI").GetComponent<UI> ();
		this.currentController = new ExplorationController (this);
	}

	// Update is called once per frame
	void Update () {
		switch (this.partyState) {

		case ePartyState.COMBAT:

			break;

		case ePartyState.EXPLORATION:

			break;

		default:
			//Should never happen.
			break;

		}
	}

	


	/**** SETTERS Y GETTERS ****/


	public ePartyState PartyState {
		get {
			return this.partyState;
		}
		set {
			partyState = value;
		}
	}

	public Combat CombatManager {
		get {
			return this.combatManager;
		}
		set {
			combatManager = value;
		}
	}

	public UI Ui {
		get {
			return this.ui;
		}
		set {
			ui = value;
		}
	}

	public DefinitionManager DefinitionManager {
		get {
			return this.definitionManager;
		}
		set {
			definitionManager = value;
		}
	}

	public Party CurrentParty {
		get {
			return this.currentParty;
		}
		set {
			currentParty = value;
		}
	}

	public Character CurrentCharacter {
		get {
			return this.currentCharacter;
		}
		set {
			currentCharacter = value;
		}
	}

	public Character Target {
		get {
			return this.target;
		}
		set {
			target = value;
		}
	}

	public GameController CurrentController {
		get {
			return this.currentController;
		}
		set {
			currentController = value;
		}
	}


}
