﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponType {

	int id;
	string name;
	string description;


	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

}
