﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item  {

	public enum EquipSlot {
		HEAD,
		BODY,
		ONEHAND,
		TWOHAND,
		MAINHAND,
		OFFHAND
	}

	private ItemDef itemDef;
	private int weight;
	private int durability;
	private ItemTrait itemTrait;

	/****   CONSTRUCTORES   ****/

	public Item (ItemDef itemDef){

		this.itemDef = itemDef;
		this.weight = itemDef.Weight;
		this.durability = itemDef.MaxDurability;
	}

	/****       METODOS     ****/

	public bool isWhole(){
		return (this.durability == this.itemDef.MaxDurability);
	}

	public bool isBroken(){
		return (this.durability == 0);
	}

	public void addDurability(int durability){
		if (!this.isWhole()){
			this.durability += durability;
			if (this.durability > this.itemDef.MaxDurability)
				this.durability = this.itemDef.MaxDurability;
		}
	}

	public void substractDurability(){
		if (!this.isBroken()){
			this.durability -= durability;
			if (this.durability < 0)
				this.durability = 0;
		}
	}

	/**** SETTERS Y GETTERS ****/

	public int Weight {
		get {
			return this.weight;
		}
		set {
			weight = value;
		}
	}

	public ItemTrait ItemTrait {
		get {
			return this.itemTrait;
		}
		set {
			itemTrait = value;
		}
	}

	public int Durability {
		get {
			return this.durability;
		}
		set {
			durability = value;
		}
	}

}
