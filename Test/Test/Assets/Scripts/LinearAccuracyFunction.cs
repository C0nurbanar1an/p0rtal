﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearAccuracyFunction : AccuracyFunction {

	int lossPerDistance;

	/****   CONSTRUCTORES   ****/



	/****       METODOS     ****/

	public override float accuracyByDistance (float distance){
		return 100 - distance * lossPerDistance;
	}

	/**** SETTERS Y GETTERS ****/



}
