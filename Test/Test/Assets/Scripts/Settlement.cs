﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settlement {

	string name;	
	List<Building> buildings;
	int maxBuildings;
	WorldPosition worldPosition;
	List<Character> hostedCharacters;
	List<Character> visitorCharacters;
	Civilization civilization;
	Sprite backgroundSprite;
	ResourceList settlementResources;
	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public void addHostedCharacter(Character character){
		this.hostedCharacters.Add (character);
	}

	public void addBuilding(Building building){
		this.buildings.Add (building);
	}

	public void removeBuilding(Building building){
		this.buildings.Remove(building);
	}

	public bool canAddBuilding(){
	
		return (this.buildings.Count < this.maxBuildings);
	
	}

	public bool tryToBuild(BuildingDef buildingDef){
		if (this.canAddBuilding ()) {
			if (this.hasEnoughResources (buildingDef.getBuildCost())) {
				this.build (buildingDef);
				return true;
			} else {
				//No tiene recursos. Notificar GUI.
				return false;
			}
		} else {
			//No hay lugar para construir. Notificar GUI.
			return false;
		}
				
	}

	private void build(BuildingDef buildingDef){

		this.substractResources (buildingDef.getBuildCost ());
		this.addBuilding (new Building (buildingDef));
		//Notifiy GUI.

	}
		
	private bool hasEnoughResources(Resources otherResources){
		return this.settlementResources.hasEnoughResources (otherResources);
	}

	public bool tryToHost(Character character){
		if (this.canHost (character)) {
			this.addHostedCharacter (character);
			return true;
		} else {
			//No hay lugar para hospedar otro personaje.
			return false;
			//Notificar GUI.
		}
	}
		
	public bool canHost (Character character){
		return (this.hostedCharacters.Count < this.maxBuildings);
	}

	public void hostCharacter(Character character){
		this.hostedCharacters.Add (character);
	}

	public bool tryToUpgradeBuilding(Building building){
		if (this.hasEnoughResources (building.getUpgradeCost())) {
			if (building.canBeUpgraded ()) {
				building.upgrade ();
				return true;
			} else {
				//building Can't be Upgraded.
				//Notify GUI.
				return false;
			}
		} else {
			//Not Enough resources to Upgrade.
			//notifyGUI.
			return false;
		}
	}

	private void upgradeBuilding(Building building){
		this.substractResources (building.getUpgradeCost ());
		building.upgrade ();
	}

	private void addResources(Resources resources){
		this.settlementResources.addResources (resources);
	}

	private void substractResources(Resources resources){
		this.settlementResources.substractResources (resources);
	}

	/**** SETTERS Y GETTERS ****/

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public int MaxBuildings {
		get {
			return this.maxBuildings;
		}
		set {
			maxBuildings = value;
		}
	}

	public List<Building> Buildings {
		get {
			return this.buildings;
		}
		set {
			buildings = value;
		}
	}

	public WorldPosition WorldPosition {
		get {
			return this.worldPosition;
		}
		set {
			worldPosition = value;
		}
	}

	public List<Character> HostedCharacters {
		get {
			return this.hostedCharacters;
		}
		set {
			hostedCharacters = value;
		}
	}

	public List<Character> VisitorCharacters {
		get {
			return this.visitorCharacters;
		}
		set {
			visitorCharacters = value;
		}
	}

	public Civilization Civilization{
		get{
			return this.civilization;
		}
		set{
			civilization = value;
		}
	}

	public Sprite BackgroundSprite{
		get{
			return this.backgroundSprite;
		}
		set{
			backgroundSprite = value;
		}
	}

	public ResourceList SettlementResources{
		get {
			return this.settlementResources;
		}
		set {
			this.settlementResources = value;
		}
	}
}