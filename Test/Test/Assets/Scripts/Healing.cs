﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healing {

	int value;
	Character healer;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Value {
		get {
			return this.value;
		}
		set {
			this.value = value;
		}
	}

	public Character Healer {
		get {
			return this.healer;
		}
		set {
			healer = value;
		}
	}
}
