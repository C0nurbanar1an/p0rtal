﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class SkillInfluence : CharacterVirtueInfluence {

	private SkillDef skillDef;
	private float multiplier;


	/****   CONSTRUCTORES   ****/

	public SkillInfluence (VirtueStatusEnum virtueStatus,
		SkillDef skillDef,
		int multiplier){

		this.virtueStatus = virtueStatus;
		this.multiplier = multiplier;
		this.skillDef = skillDef;

	}

	public SkillInfluence(){

	}

	/****       METODOS     ****/

	public override float getCharacterInfluence(Character character){

		Skills skills = getProperSkillsFromCharacter (character);
		Skill sk = skills.getSkillById(this.skillDef.Id);

		if (sk != null) {
			//Debug.LogWarning ("Skill Found : " + sk.SkillDef.Name + " - VALUE : " + sk.Value);
			return (int) (sk.Value * this.multiplier);

		} else {
			Debug.LogWarning ("Skill " + this.skillDef.Id + " Not found!");
			return 0.0f;
			//Debug.LogError ("Skill " + this.skillDef.Key + " Not Found on Character " + character.ToString());
		}

	}

	private Skills getProperSkillsFromCharacter(Character character){
		Skills properSkills;
		switch(this.virtueStatus){
		case VirtueStatusEnum.BASE:
			properSkills = character.CharacterSkills.BaseSkills;
			break;
		case VirtueStatusEnum.EXTRA:
			properSkills = character.CharacterSkills.ExtraSkills;
			break;
		case VirtueStatusEnum.FINAL:
			properSkills = character.CharacterSkills.FinalSkills;
			break;
		default:
			properSkills = null; //Error.
			Debug.LogError ("invalid virtueStatus!");
			break;
		}
			
		return properSkills;

	}

	public override string ToString ()
	{
		string result = "SkillInfluence : " + Environment.NewLine;
		result += "SkillDef : " + this.skillDef.Key + Environment.NewLine;
		result += "Status : " + Enum.GetName (typeof(VirtueStatusEnum), this.virtueStatus) + Environment.NewLine;
		result += "Multiplier : " + this.multiplier + Environment.NewLine;
		return result;
	}

	public string toSmallString(){

		return "[SKILL] (" + Enum.GetName (typeof(VirtueStatusEnum),this.virtueStatus) + ") " + this.skillDef.Key + " X " + this.multiplier;

	}

	/**** SETTERS Y GETTERS ****/

	public SkillDef SkillDef {
		get {
			return this.skillDef;
		}
		set {
			skillDef = value;
		}
	}

	public float Multiplier {
		get {
			return this.multiplier;
		}
		set {
			multiplier = value;
		}
	}
}
