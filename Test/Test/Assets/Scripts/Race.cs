﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Race {

	int id;
	string name;
	string description;
	Biome nativeBiome;
	Biology biology;
	List<Ability> racialAbilites;
	Attributes raceAttributeModifiers;
	//List<Skill> skills;
	//raceGraphics graphics;


	/****   CONSTRUCTORES   ****/

	public Race(){
		this.racialAbilites = new List<Ability> ();
		this.raceAttributeModifiers = new Attributes ();
	}

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

	public Biome NativeBiome {
		get {
			return this.nativeBiome;
		}
		set {
			nativeBiome = value;
		}
	}

	public Biology Biology {
		get {
			return this.biology;
		}
		set {
			biology = value;
		}
	}

	public List<Ability> RacialAbilites {
		get {
			return this.racialAbilites;
		}
		set {
			racialAbilites = value;
		}
	}

	public Attributes RaceAttributeModifiers {
		get {
			return this.raceAttributeModifiers;
		}
		set {
			raceAttributeModifiers = value;
		}
	}

}