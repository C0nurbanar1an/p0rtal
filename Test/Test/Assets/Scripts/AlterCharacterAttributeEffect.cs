﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class AlterCharacterAttributeEffect : CharacterEffect {

	AttributeDef attributeDef;

	/****   CONSTRUCTORES   ****/

	public AlterCharacterAttributeEffect(){
		this.type = EffectType.ALTER_ATTRIBUTE;
	}

	/****       METODOS     ****/

	public override void apply(Character caster, Character target){
		Attribute attributeToAdd = this.getAttributeToAdd (caster, target);
		target.addExtraAttribute (attributeToAdd);	

		//Debug.Log ("AlterAttribute applied to " + target.Biography.Name);
	}

	public Attribute getAttributeToAdd(Character caster, Character target){
		Attribute attr = new Attribute (this.attributeDef);
		attr.Value = this.getFormulaValue (caster, target);
		return attr;
	}

	public int getDeltaValue(Character caster, Character target){

		return this.formula.getFormulaValue(caster,target);

	}

	public override string ToString(){

		string result = "Alter Attribute Effect : " + Environment.NewLine;
		result += "Attribute : " + this.attributeDef.Key + Environment.NewLine;
		result += this.formula.ToString() + Environment.NewLine;
		return result;
	}
	/**** SETTERS Y GETTERS ****/

	public AttributeDef AttributeDef {
		get {
			return this.attributeDef;
		}
		set {
			attributeDef = value;
		}
	}

}

