﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tech  {

	/**** Esta clase se crea a traves de sus constructores particulares, y se construiran
	 * siguiendo un patron estilo Factoria.  (TechDef tiene un metodo para generar una Tech)
	 * Sus atributos no tienen setter, solo se manejan a traves de AddScience y sus constructores
	 * para mantener la integridad de sus miembros. */

	private TechDef techDef;
	private int scienceCollected;
	private bool researchCompleted;

	/****   CONSTRUCTORES   ****/

	public Tech (TechDef techDef){
		this.techDef = techDef;
		this.scienceCollected = 0;
		this.researchCompleted = (techDef.ScienceCost == 0);
	}

	public Tech (TechDef techDef, int scienceCollected){
		this.techDef = techDef;
		this.addScience (scienceCollected);
	}

	/****       METODOS     ****/

	public void addScience(int science){
		this.scienceCollected += science;
		if (this.scienceCollected > this.techDef.ScienceCost){
			this.scienceCollected = this.techDef.ScienceCost;
			this.researchCompleted = true;
		}
	}

	/**** SETTERS Y GETTERS ****/

	public TechDef TechDef {
		get {
			return this.techDef;
		}
	}

	public int ScienceCollected {
		get {
			return this.scienceCollected;
		}
	}

	public bool ResearchCompleted {
		get {
			return this.researchCompleted;
		}
	}

}
