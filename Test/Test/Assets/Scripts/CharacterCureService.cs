﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCureService : Service {

	private Buff buff;
	private int progressGoal;
	private int progress;
	private Character character;
	/****   CONSTRUCTORES   ****/

	public CharacterCureService(Buff buff){
		this.buff = buff;
		this.progressGoal = buff.getCureCost();
		this.progress = 0;
		this.minWorkers = 0;
	}

	/****       METODOS     ****/

	protected override void onCompleted (){
		this.character.CharacterBuffs.removeBuff (this.buff);
		//Notify.
	}

	protected override void updateProgress(){
		this.addProgressToService (this.ProgressPerMinutes(1));
	}

	public override int progressPerMinute(){
		int total = 0;
		foreach (Character c in this.workers){
			total += c.CharacterSkills.FinalSkills.getSkillByKey ("CRF").Value;
		}
		return total;
	}

	protected override bool isCompleted(){
		return (this.progress >= this.progressGoal);
	}

	protected override void addProgress(int progress){
		this.progress += progress;
	}

	/**** SETTERS Y GETTERS ****/

	public Buff Buff {
		get {
			return this.buff;
		}
		set {
			buff = value;
		}
	}

	public int ProgressGoal {
		get {
			return this.progressGoal;
		}
		set {
			progressGoal = value;
		}
	}

	public int Progress {
		get {
			return this.progress;
		}
		set {
			progress = value;
		}
	}

}
