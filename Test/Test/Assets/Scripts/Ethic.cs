﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ethic {

	public enum EthicKey {
		PACIFIST,
		BELICIST,
		TRADER,
		DIPLOMATIC
	}

	EthicKey key;
	string name;
	string description;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public EthicKey Key {
		get {
			return this.key;
		}
		set {
			key = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public string Description {
		get {
			return this.description;
		}
		set {
			description = value;
		}
	}

}