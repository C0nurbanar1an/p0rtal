﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetMouse : MonoBehaviour {
	public SimulationScript simulator;

	// Use this for initialization
	void Start () {
		simulator = GameObject.Find("Simulator").GetComponent<SimulationScript>();	

	}

	
	// Update is called once per frame
	void Update () {
		transform.position = simulator.targetPosition;
	}
}
