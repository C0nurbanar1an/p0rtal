﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Civilization {
	private string name;
	private string background;
	private Race nativeRace;
	private List<Ethic> ethics;
	private CivTechs civTechs;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/


	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public Race NativeRace {
		get {
			return this.nativeRace;
		}
		set {
			nativeRace = value;
		}
	}

	public List<Ethic> Ethics {
		get {
			return this.ethics;
		}
		set {
			ethics = value;
		}
	}

	public string Background {
		get {
			return this.background;
		}
		set {
			background = value;
		}
	}

	public CivTechs CivTechs {
		get {
			return this.civTechs;
		}
		set {
			civTechs = value;
		}
	}

}