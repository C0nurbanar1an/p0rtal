﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.CompilerServices;


public class Character {

	private Race race;
	private Inventory inventory;
	private CharacterConditions characterConditions;
	private Civilization civilization;
	private int age;
	private Biography biography;
	private WorldPosition worldPosition;
	private CharacterAttributes characterAttributes;
	private CharacterSkills characterSkills;
	private List<CharacterTrait> characterTraits;
	private Attributes geneticsAttributes;
	private CharacterBuffs characterBuffs;
	private CharacterMB characterMB;
	private List<Ability> quickAbilities;
	private Party party;
	private bool isWorking;

	private const int SLOT_MAX_INDEX = 9;

	/****   CONSTRUCTORES   ****/

	public Character(DefinitionManager definitions){
		this.biography = new Biography ();
		this.race = new Race ();
		this.characterTraits = new List<CharacterTrait> ();
		this.geneticsAttributes = new Attributes (false,definitions);
		this.characterAttributes = new CharacterAttributes (this,definitions);
		this.characterSkills = new CharacterSkills (this,definitions);
		this.characterConditions = new CharacterConditions (this,definitions);
		this.characterBuffs = new CharacterBuffs (this);
		this.quickAbilities = new List<Ability>(10);

	}

	/****       METODOS     ****/

	public void onTimePassed(int seconds){
		this.characterBuffs.onTimePassed (seconds);
	}

	public void addBuff(Buff buff){
		this.characterBuffs.addBuff (buff);
	}

	public void addExtraSkill(Skill skill){
		this.characterSkills.addExtraSkill (skill);
		this.onVirtuesUpdated ();
	}

	public void addExtraAttribute(Attribute attribute){
		this.characterAttributes.addExtraAttribute (attribute);
		this.characterSkills.attributeUpdated (attribute.AttributeDef);
		this.onVirtuesUpdated ();
	}

	private void onVirtuesUpdated (){
		this.CharacterConditions.updateBoundaries ();
	}

	public void recalculateVirtues(){
		this.characterAttributes.initialize ();
		this.characterSkills.initialize ();
		this.applyBuffConstantEffects ();
		//this.
	}

	public void applyBuffConstantEffects(){
		this.characterBuffs.applyBuffConstantEffects ();
	}

	public void setQuickAbility(Ability ability,int slot){
		this.quickAbilities [slot] = ability;
	}

	public Ability getQuickAbility(int slot){
		if (slot < 0 || slot > SLOT_MAX_INDEX) {
			Debug.LogError ("Quick Ability Slot " + slot + " Out of boundaries!");
			return null;
		} else {
			return this.quickAbilities [slot];
		}

	}

	public void receiveDamage(Damage damage){

		//TODO : Contemplar daño en HP y daño en Armor.

		this.characterConditions.substractValueFromCondition ("HP", damage.Value);
		this.characterMB.floatDamageReceived (damage.Value);

	}

	public void receiveHealing(Healing healing){
		//TODO Completar cuestiones del Healing.
		this.characterConditions.addValueToCondition ("HP", healing.Value);

	}

	/*
	public void applyEffect(CharacterEffect effect){
		effect.apply (this);
		this.effectApplied (effect);
	}
	*/

	public void effectApplied (CharacterEffect effect){

		//Notify GUI.
	
	}

	public void castAbility(Ability ability, Character target){
		if (this.canCastAbility(ability)){
			if ((float) ability.AbilityDef.Range >= this.distance (target.getPosition ())) {
				
				this.CharacterConditions.substractConditionValues (ability.ConditionCost);
				ability.cast (this, target);

			} else {
				//Notifiar que esta fuera de Rango.
				Debug.LogWarning("Out of Range - " + ability.AbilityDef.Name + " range is " + ability.AbilityDef.Range
					+ " and distance from target is " + this.distance(target.getPosition()));
			}
		}
	}

	public float distance(Vector3 position){
		Debug.LogWarning ("Distance : " + Vector3.Distance (this.getPosition (), position));
		return Vector3.Distance (this.getPosition(), position);
	}

	public bool hasEnoughConditions(Dictionary <CharacterConditionDef,int> abilityConditions){

		CharacterConditionDef conditionDef;
		int value;

		foreach (KeyValuePair<CharacterConditionDef,int> keyValue in abilityConditions) {
			conditionDef = keyValue.Key;
			value = keyValue.Value;

			if (this.characterConditions.getCharacterCondition (conditionDef).Value < value) {
				return false;
			}
		}
		return true;

	}

	public bool canCastAbility (Ability ability){

		bool result = true;

		//Evaluar costes de AP y conditions.
		if (!this.hasEnoughConditions(ability.AbilityDef.CharacterConditionCost)){
			this.characterMB.GameManager.printConsoleMsg ("Not enough Conditions to cast " + ability.AbilityDef.Name);
			result = false;
		}

		//Cooldown
		if (ability.RemainingCooldown > 0) {
			this.characterMB.GameManager.printConsoleMsg (ability.AbilityDef.Name + " is still on Cooldown");
			result = false;
		}

		return result;

	}
	
	
	public void notifyStartedWorking(){
		this.isWorking = true;
	}

	public void notifyStoppedWorking(){
		this.isWorking = false;
	}

	public Attributes getTraitsAttributeModifiers(){
		Attributes attr = new Attributes ();
		foreach (CharacterTrait ct in this.characterTraits){
			attr.addAttributes (ct.AttributeModifiers);
		}
		return attr;
	}

	public Attributes getRaceAttributeModifiers(){
		return this.Race.RaceAttributeModifiers;
	}

	public Vector3 getPosition (){
		return this.characterMB.transform.position;
	}

	public void instantiateOnScene(Vector3 mapPosition){
		Vector3 goPosition;

		if (mapPosition == null) {
			goPosition = new Vector3 (0, 1, 0);
		} else {
			goPosition = mapPosition;
		}

		GameObject go = GameObject.Instantiate ((GameObject) Resources.Load ("Prefabs/CharacterPrefabQuad"));
		go.transform.position = mapPosition;
		go.name = "Character " + this.biography.Name;
		CharacterMB charMB = go.AddComponent<CharacterMB> ();

		charMB.setCharacter (this);

	}

	public bool isAlive(){
		return (this.characterConditions.getCharacterCondition ("HP").Value > 0);
	}

	public bool isDead(){
		return (!this.isAlive ());
	}

	public bool isHardCCed(){
		//TODO : Implementar Buffs que apliquen CCs (Estados).
		//TODO : Tambien buscarle un nombre mas apropiado.
		return false;
	}

	public bool tryToAddToParty(Party party){
		bool wasAdded = party.tryToAddCharacter (this);
		if (wasAdded) {
			this.party = party;
			return true;
		} else {
			return false;
		}
	}
	/**** SETTERS Y GETTERS ****/


	public Race Race {
		get {
			return this.race;
		}
		set {
			race = value;
		}
	}

	public Inventory Inventory {
		get {
			return this.inventory;
		}
		set {
			inventory = value;
		}
	}

	public CharacterConditions CharacterConditions {
		get {
			return this.characterConditions;
		}
		set {
			characterConditions = value;
		}
	}

	public Civilization Civilization {
		get {
			return this.civilization;
		}
		set {
			civilization = value;
		}
	}

	public int Age {
		get {
			return this.age;
		}
		set {
			age = value;
		}
	}

	public Biography Biography {
		get {
			return this.biography;
		}
		set {
			biography = value;
		}
	}

	public WorldPosition WorldPosition {
		get {
			return this.worldPosition;
		}
		set {
			worldPosition = value;
		}
	}

	public CharacterAttributes CharacterAttributes {
		get {
			return this.characterAttributes;
		}
		set {
			characterAttributes = value;
		}
	}

	public CharacterSkills CharacterSkills {
		get {
			return this.characterSkills;
		}
		set {
			characterSkills = value;
		}
	}

	public List<CharacterTrait> CharacterTraits {
		get {
			return this.characterTraits;
		}
		set {
			characterTraits = value;
		}
	}

	public Attributes GeneticsAttributes {
		get {
			return this.geneticsAttributes;
		}
		set {
			geneticsAttributes = value;
		}
	}

	public CharacterBuffs CharacterBuffs {
		get {
			return this.characterBuffs;
		}
		set {
			characterBuffs = value;
		}
	}

	public CharacterMB CharacterMB {
		get {
			return this.characterMB;
		}
		set {
			characterMB = value;
		}
	}

	public Party Party {
		get {
			return this.party;
		}
		set {
			party = value;
		}
	}

	public class InitiativeComparer : IComparer<Character>
	{
		public int Compare(Character x, Character y){
			int xInitiative = x.characterSkills.FinalSkills.getSkillByKey ("INI").Value;
			int yInitiative = x.characterSkills.FinalSkills.getSkillByKey ("INI").Value;
			return (xInitiative - yInitiative);
		}
	}
}
