﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaunaSpecies {

	int id;
	string name;
	float chanceToExistPerTile;
	bool livesInPack;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	/**** SETTERS Y GETTERS ****/

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public float ChanceToExistPerTile {
		get {
			return this.chanceToExistPerTile;
		}
		set {
			chanceToExistPerTile = value;
		}
	}

	public bool LivesInPack {
		get {
			return this.livesInPack;
		}
		set {
			livesInPack = value;
		}
	}

}