﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class CharacterEffect {

	public enum EffectType{
		DEAL_DAMAGE,
		HEAL_CHARACTER,
		ALTER_ATTRIBUTE,
		ALTER_SKILL,
		ALTER_CONDITION,
		ADD_BUFF
	}

	protected EffectType type;
	protected CharacterEffectFormula formula;

	/****   CONSTRUCTORES   ****/

	/****       METODOS     ****/

	public bool isConstantEffect(){
		return (this.type == EffectType.ALTER_ATTRIBUTE || this.type == EffectType.ALTER_SKILL);
	}

	//Esta se overridea para aplicar los distintos efectos
	//A traves de las subclases.
	public virtual void apply(Character caster, Character target){
		
	}

	public virtual string toString(){
		return "EFECTO GENERICO. ESTO NO DEBERIA APARECER";
	}

	public int getFormulaValue(Character caster, Character target){

		return formula.getFormulaValue (caster, target);

	}


	/**** SETTERS Y GETTERS ****/

	public CharacterEffectFormula Formula {
		get {
			return this.formula;
		}
		set {
			formula = value;
		}
	}

	public EffectType Type {
		get {
			return this.type;
		}
		set {
			type = value;
		}
	}



}
