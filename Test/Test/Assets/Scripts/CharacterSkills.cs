﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSkills {

	private Character character;
	private Skills baseSkills;
	private Skills extraSkills;
	private Skills finalSkills;

	/****   CONSTRUCTORES   ****/

	public CharacterSkills (Character character,DefinitionManager definitions){
		this.character = character;
		this.baseSkills = new Skills (true,definitions);
		this.extraSkills = new Skills (false,definitions);
		this.finalSkills = new Skills (true,definitions);
		this.initialize ();
	}

	/****       METODOS     ****/

	public void attributeUpdated(AttributeDef attributeDef){
		//Considerar implementar IEnumerable en Skills.
		foreach (Skill sk in this.baseSkills.SkillList) {
			if (sk.SkillDef.AttributeFormula.hasAttribute (attributeDef)) {
				//Debug.LogWarning ("Updating Skill : " + sk.SkillDef.Name + "..." );
				this.updateBaseSkill (sk.SkillDef);
				this.updateFinalSkill (sk.SkillDef);
			}
		}
	}

	public void addExtraSkill(Skill skill){
		this.extraSkills.addSkill (skill);
		this.updateFinalSkill (skill.SkillDef);
	}

	public void initialize(){
		this.initializeSkills ();
	}
		
	public void initializeSkills(){
		this.updateBaseSkills ();
		this.extraSkills.reset ();
		this.updateFinalSkills ();
	}

	public void resetSkills(){
		this.baseSkills.reset ();
		this.extraSkills.reset ();
		this.finalSkills.reset ();
	}

	public void updateBaseSkill(SkillDef skd){
		Skill baseSkill = this.baseSkills.getSkill (skd);
		if (baseSkill != null) {
			//Debug.LogWarning ("Setting Skill : " + skd.Name + " Value by Formula..." );
			baseSkill.setValueByFormula (this.character);
		}
	}

	public void updateSkills(){
		this.updateBaseSkills ();
		this.calculateExtraSkills();
		this.updateFinalSkills ();
	}

	private void calculateExtraSkills(){
		this.extraSkills.clear ();
		//Falta proceso.
	}

	private void updateBaseSkills(){
		//No hace falta clear porque reasignamos todos.
		this.calculateBaseSkills ();
	}

	private void calculateBaseSkills(){
		foreach (Skill s in this.baseSkills.SkillList){
			s.Value = s.SkillDef.AttributeFormula.calculateValue (this.character.CharacterAttributes.FinalAttributes);
			//Debug.LogWarning ("Skill " + s.SkillDef.Name + "Base Value : " + s.Value);
		}
	}

	private void updateFinalSkills (){
		this.finalSkills.reset ();
		finalSkills.addSkills (this.baseSkills);
		finalSkills.addSkills (this.extraSkills);
	}

	private void updateFinalSkill(SkillDef skd){
		this.updateFinalSkill (this.finalSkills.getSkill (skd));	
	}

	private void updateFinalSkill(Skill finalSkill){
		Skill extraSkill = this.extraSkills.getSkill (finalSkill.SkillDef);
		Skill baseSkill = this.baseSkills.getSkill (finalSkill.SkillDef);

		if (finalSkill != null) {
			finalSkill.reset ();
			if (baseSkill != null) {
				finalSkill.addSkill(baseSkill);
			}
			if (extraSkill != null) {
				finalSkill.addSkill (extraSkill);
			}
		}
	}

	public string getFinalSkillList(){
		return this.finalSkills.getSkillList ();
	}

	/**** SETTERS Y GETTERS ****/

	public Character Character {
		get {
			return this.character;
		}
		set {
			character = value;
		}
	}

	public Skills BaseSkills {
		get {
			return this.baseSkills;
		}
		set {
			baseSkills = value;
		}
	}

	public Skills ExtraSkills {
		get {
			return this.extraSkills;
		}
		set {
			extraSkills = value;
		}
	}

	public Skills FinalSkills {
		get {
			return this.finalSkills;
		}
		set {
			finalSkills = value;
		}
	}

}
