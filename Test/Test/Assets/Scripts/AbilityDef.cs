﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;


public class AbilityDef : Definition  {

	public enum AbilityTargetType {
		SELF_CAST, 
		CHAR_TARGETED,
		GROUND_TARGETED,
		NULL
	};	


	SkillDef skillDef; //SkillDef this ability belongs to. can be Null.
	int baseCooldown;
	int apCost;
	Dictionary <CharacterConditionDef,int> characterConditionCost;
	AbilityTargetType targetType;
	List<AbilityDef> requiredAbilites;
	Sprite sprite;
	int range;

	List<CharacterEffect> casterEffects;
	List<CharacterEffect> targetEffects;

	/****   CONSTRUCTORES   ****/

	public AbilityDef(){
		this.requiredAbilites = new List<AbilityDef>();
		this.characterConditionCost = new Dictionary<CharacterConditionDef, int>();
		this.casterEffects = new List<CharacterEffect>();
		this.targetEffects = new List<CharacterEffect> ();
	}

	/****       METODOS     ****/

	public bool tryToCast(){
		//Evaluar con los costes de AP y de Conditions.
		return true;
	}

	public void cast(Character caster, Character target){


		this.applyEffects (caster, target);


	}

	private void applyEffects(Character caster, Character target){

		foreach (CharacterEffect ce in this.casterEffects) {
			ce.apply (caster, target);
		}

		foreach (CharacterEffect ce in this.targetEffects) {
			ce.apply (caster, target);
		}

	}

	public void addCharacterConditionCost(CharacterConditionDef def, int cost){
		this.characterConditionCost.Add (def,cost);
	}

	public override string ToString(){
		string result = "AbilityDef (" + this.key + ") : " + Environment.NewLine
		+ "ID : " + this.id + Environment.NewLine
		+ "Name : " + this.name + Environment.NewLine
		+ "Description : " + this.description + Environment.NewLine
			+ "Target Type : " + Enum.GetName (typeof(AbilityTargetType), this.targetType) + Environment.NewLine;
		if (this.characterConditionCost.Count > 0) {
			result += this.characterConditionCostsToString () + Environment.NewLine;
		}
		if (this.casterEffects.Count > 0)
			result += "Effects on Caster : " + this.effectsToString (this.casterEffects) + Environment.NewLine;
		
		if (this.targetEffects.Count > 0)
			result += "Effects on Target : " + this.effectsToString (this.targetEffects) + Environment.NewLine;
		return result + Environment.NewLine + Environment.NewLine;
	}

	private string characterConditionCostsToString(){
		string result = "Condition Costs : ";
		foreach (KeyValuePair<CharacterConditionDef,int> keyValue in this.characterConditionCost) {
			result += Environment.NewLine + "    " +  keyValue.Key.Name + " : " + keyValue.Value;
		}
		return result;
	}

	private string effectsToString(List<CharacterEffect> effects){
		string result = "" + Environment.NewLine;
		foreach (CharacterEffect ce in effects) {
			result += ce.ToString () + Environment.NewLine;
		}
		return result;
	}
		

	public void addCasterEffect(CharacterEffect effect){

		this.casterEffects.Add (effect);

	}

	public void addTargetEffect(CharacterEffect effect){

		this.targetEffects.Add (effect);

	}

	/**** SETTERS Y GETTERS ****/

	public SkillDef SkillDef {
		get {
			return this.skillDef;
		}
		set {
			skillDef = value;
		}
	}

	public int BaseCooldown {
		get {
			return this.baseCooldown;
		}
		set {
			baseCooldown = value;
		}
	}

	public Dictionary<CharacterConditionDef, int> CharacterConditionCost {
		get {
			return this.characterConditionCost;
		}
		set {
			characterConditionCost = value;
		}
	}

	public int ApCost {
		get {
			return this.apCost;
		}
		set {
			apCost = value;
		}
	}

	public AbilityTargetType TargetType {
		get {
			return this.targetType;
		}
		set {
			this.targetType = value;
		}
	}

	public List<AbilityDef> RequiredAbilites {
		get {
			return this.requiredAbilites;
		}
		set {
			requiredAbilites = value;
		}
	}

	public List<CharacterEffect> CasterEffects {
		get {
			return this.casterEffects;
		}
		set {
			casterEffects = value;
		}
	}

	public List<CharacterEffect> TargetEffects {
		get {
			return this.targetEffects;
		}
		set {
			targetEffects = value;
		}
	}

	public Sprite Sprite {
		get {
			return this.sprite;
		}
		set {
			sprite = value;
		}
	}

	public int Range {
		get {
			return this.range;
		}
		set {
			range = value;
		}
	}

}
