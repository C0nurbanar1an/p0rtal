﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplorationController : GameController {

	public ExplorationController (GameManager gameManager){
		this.gameManager = gameManager;
		this.ui = gameManager.Ui;
		this.start ();
		this.updateUI ();

	}

	/****   CONSTRUCTORES   ****/



	/****       METODOS     ****/

	protected override void updateUI(){
		this.setCombatModeButtonSpriteToCombat ();
		this.ui.hideEndTurnButton ();
	}

	private void setCombatModeButtonSpriteToCombat(){

		this.ui.setCombatModeButtonSprite (DefinitionManager.loadSprite ("warcraftSprites2_18"));

	}


	public override void onUpCharacterPressed(){
		this.selectNextPartyCharacter ();
	}

	public override void onDownCharacterPressed(){
		this.selectPreviousPartyCharacter ();
	}

	public void selectNextPartyCharacter(){
		int currentIndex = this.gameManager.CurrentParty.getCharacterIndex (this.gameManager.CurrentCharacter);
		if (currentIndex < this.gameManager.CurrentParty.Characters.Count - 1) {
			this.gameManager.CurrentCharacter = this.gameManager.CurrentParty.getCharacterAtIndex (currentIndex + 1);
		}
	}

	public void selectPreviousPartyCharacter(){
		int currentIndex = this.gameManager.CurrentParty.getCharacterIndex (this.gameManager.CurrentCharacter);
		if (currentIndex > 0) {
			this.gameManager.CurrentCharacter = this.gameManager.CurrentParty.getCharacterAtIndex (currentIndex - 1);
		}
	}

	public override void characterOnMouseEnter(CharacterMB characterMB){		

		characterMB.enableOutline (UI.BLUE);

		this.ui.showTargetPanel (characterMB);

	}

	public override void characterOnMouseExit(CharacterMB characterMB){
		this.ui.hideTargetPanel ();
	}

	protected override void start(){

	}

	protected  override void update(){

	}

	public override void onCombatButtonPressed(){
		this.gameManager.enterCombatMode ();

	}

	/**** SETTERS Y GETTERS ****/

}
