﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class DealDamageEffect : CharacterEffect{

	DamageType damageType;

	/****   CONSTRUCTORES   ****/

	public DealDamageEffect (){
		this.type = EffectType.DEAL_DAMAGE;
	}

	/****       METODOS     ****/

	public override void apply(Character caster,
							   Character target){
		//Validar nulls y eso.
		target.receiveDamage (this.createDamage (caster, target));

	}


	public Damage createDamage(Character caster,
								 Character target){
		Damage damage = new Damage ();
		damage.Value = this.getDamageValue(caster,target);
		damage.DamageDealer = caster;
		return damage;
	}

	private int getDamageValue(Character caster,
								Character target){
		return this.formula.getFormulaValue(caster,target);
	}

	public override string ToString (){

		string result = "Deal Damage Effect : " + Environment.NewLine;
		result += "Damage Type : " + this.damageType.Name + Environment.NewLine;
		result += this.formula.ToString ();

		return result;
	}

	/**** SETTERS Y GETTERS ****/

	public DamageType DamageType {
		get {
			return this.damageType;
		}
		set {
			damageType = value;
		}
	}

}
