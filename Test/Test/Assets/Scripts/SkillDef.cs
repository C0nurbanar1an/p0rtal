﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Schema;
using System;

public class SkillDef : Definition {

	public const int SKILL_MAX_LVL = 10;
	private static readonly int[] lvlExpMilestonesArray = 	{500,1500,3000,5000,7500, 10500, 14500, 20000, 26000, 32500};

	private List<int> lvlExpMilestones;
	private AttributeFormula attributeFormula;
	private AbilityMap abilityMap;
	private int maxLvl = SKILL_MAX_LVL;

	/****   CONSTRUCTORES   ****/

	public SkillDef(){
		this.lvlExpMilestones = new List<int> (SkillDef.lvlExpMilestonesArray);
	}
		
	/****       METODOS     ****/

	public override string ToString(){
		return "SkillDef (" + this.key + ") : " + Environment.NewLine
			+ "ID : " + this.id + Environment.NewLine
			+ "Name : " + this.name + Environment.NewLine
			+ "Description : " + this.description + Environment.NewLine
			+ "Attribute Formula : " + this.attributeFormula.ToString() + Environment.NewLine + Environment.NewLine;
	}

	public int expToLvl(int exp){
		int i;
		for (i = this.maxLvl - 1;i >= 0 ; i--){
			if (exp >= this.lvlExpMilestones[i]){
				return i + 1;
			}
		}
		return 0;
	}

	/**** SETTERS Y GETTERS ****/

	public List<int> LvlExpMilestones {
		get {
			return this.lvlExpMilestones;
		}
		set {
			lvlExpMilestones = value;
		}
	}

	public AttributeFormula AttributeFormula {
		get {
			return this.attributeFormula;
		}
		set {
			attributeFormula = value;
		}
	}

	public AbilityMap AbilityMap {
		get {
			return this.abilityMap;
		}
		set {
			abilityMap = value;
		}
	}



}

