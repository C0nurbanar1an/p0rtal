﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anatomy {

	int id;
	string name;
	int armCount;
	int legCount;

	public int Id {
		get {
			return this.id;
		}
		set {
			id = value;
		}
	}

	public string Name {
		get {
			return this.name;
		}
		set {
			name = value;
		}
	}

	public int ArmCount {
		get {
			return this.armCount;
		}
		set {
			armCount = value;
		}
	}

	public int LegCount {
		get {
			return this.legCount;
		}
		set {
			legCount = value;
		}
	}
}
