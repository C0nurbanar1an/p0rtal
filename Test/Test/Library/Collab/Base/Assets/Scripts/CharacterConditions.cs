﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterConditions {

	Character character;
	List<CharacterCondition> characterConditions;

	/****   CONSTRUCTORES   ****/

	public CharacterConditions(Character character, DefinitionManager definitions){
		this.character = character;
		this.characterConditions = new List<CharacterCondition> ();
		this.addAllDefinedConditions (definitions);
	}

	public CharacterConditions(){
		this.characterConditions = new List<CharacterCondition> ();
	}

	/****       METODOS     ****/

	private void addAllDefinedConditions (DefinitionManager definitions){
		foreach (CharacterConditionDef ccd in definitions.ConditionDefinitions){
			this.addCharacterCondition (ccd);
			//Debug.LogWarning ("Condition Added : " + ccd.Name);
		}
	}

	public void addCharacterCondition(CharacterConditionDef characterConditionDef){
		this.characterConditions.Add (new CharacterCondition (characterConditionDef,this));
	}

	public void addCharacterCondition(CharacterCondition characterCondition){
		this.characterConditions.Add (characterCondition);
	}

	public void alterConditionValues(Dictionary<CharacterConditionDef,int> conditionDeltas){
		foreach (KeyValuePair<CharacterConditionDef,int> keyValue in conditionDeltas) {
			this.alterConditionValue (keyValue.Key,keyValue.Value);
		}
	}

	public void alterConditionValue(CharacterConditionDef ccd,int value){
		CharacterCondition cd = this.getCharacterCondition (ccd.Id);
		if (cd != null) {
			cd.alterValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public void alterConditionValue(string key,int value){
		CharacterCondition cd = this.getCharacterCondition (key);
		if (cd != null) {
			cd.alterValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public void addValueToCondition(string key, int value){
		CharacterCondition cd = this.getCharacterCondition (key);
		if (cd != null) {
			cd.addValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public void addValueToCondition(int id, int value){
		CharacterCondition cd = this.getCharacterCondition (id);
		if (cd != null) {
			cd.addValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public void substractValueFromCondition(int id,int value){
		CharacterCondition cd = this.getCharacterCondition (id);
		if (cd != null) {
			cd.substractValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public void substractValueFromCondition(string key,int value){
		CharacterCondition cd = this.getCharacterCondition (key);
		if (cd != null) {
			cd.substractValue (value);
			//Evaluar buffs y esas cosas.
		} else {
			//No se encontro esa Condition.
		}
	}

	public CharacterCondition getCharacterCondition(string key){
		foreach (CharacterCondition cd in this.characterConditions){
			if (cd.CharacterConditionDef.Key == key) {
				return cd;
			}
		}
		return null;
	}

	public CharacterCondition getCharacterCondition(int id){
		foreach (CharacterCondition cd in this.characterConditions){
			if (cd.CharacterConditionDef.Id == id) {
				return cd;
			}
		}
		return null;
	}

	public CharacterCondition getCharacterCondition(CharacterConditionDef ccd){
		foreach (CharacterCondition cd in this.characterConditions){
			if (cd.CharacterConditionDef.Id == ccd.Id) {
				return cd;
			}
		}
		return null;
	}

	public void updateBoundaries(){
		foreach (CharacterCondition cd in this.characterConditions) {
			cd.updateBoundaries ();
		}
	}

	/**** SETTERS Y GETTERS ****/

	public Character Character {
		get {
			return this.character;
		}
		set {
			character = value;
		}
	}

	public List<CharacterCondition> conditions {
		get {
			return this.characterConditions;
		}
		set {
			characterConditions = value;
		}
	}


}